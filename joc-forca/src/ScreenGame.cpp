/*
 * ScreenGame.cpp
 *
 *  Created on: 24/02/2014
 *      Author: xaviersuaucuadros
 */


#include "ScreenGame.h"

//--------------------------------------------------------------
void ScreenGame::setup(){


	/*
	 * init common vars
	 */
	NatScreen::initCommonVars();

		life = INIT_LIFE;
		points = 0;

		leaves_bottom.loadImage("../../../common-assets/plantes_baix.png");
		leaves_top.loadImage("../../../common-assets/plantes_dalt.png");


	/*
	 * init specific vars
	 */

	/// LOAD IMAGES
	bear_bg.loadImage("game/os-blanc-negre.png");
	bear.loadImage("game/os-color.png");
	termometer_bg.loadImage("game/termometre.png");
	podium.loadImage("game/podium.png");

	temperature = std::vector<ofImage>(11);
	for( int i=0; i<=10; i++ )
	{
		std::stringstream ss;
		ss<<"game/barra_termometre/"<<i*10<<".png";
		temperature[i].loadImage( ss.str() );
	}

	//Set serial connection
	strength = 0;

	int baud = 19200;
	#ifdef linux
	std::cout<<"Searching serial /dev/ttyACM0"<<std::endl;
		serial.setup( "/dev/ttyUSB0", baud ); //Change depending on computer
#else
		serial.setup( "/dev/tty.usbmodemfa141", baud ); //Change depending on computer
#endif

//Load rope max strength config
	ofxXmlSettings settings;
	settings.loadFile("strings.xml");
	max_strength = settings.getValue("GAMECONF:max_strength", 100.0);
	std::cout<<"MAX STRENGTH = "<<max_strength<<std::endl;

show_strength = 0;
best_pull = 0;
did_pull = false;

ZeroTime();

	ofSetVerticalSync(true);
	ofSetFrameRate(30);
	ofBackground(255,255,255);

}

void ScreenGame::load_language_data()
{
	loadCommonStrings(language);

	//now load that same file and get the values out
	ofxXmlSettings settings;
	settings.loadFile("strings.xml");
	explanationStr = settings.getValue(language + ":explanation", "Mou la mà per jugar!");
}


//--------------------------------------------------------------
void ScreenGame::update(){



	next_screen = ScreenInfo();


	if( !haveWinner()   )
	{
		/* read new data from serial */
		std::string newData;
		//bool readOk = serial.readUntil(newData, '\n');
		newData = serial.readLine( '\n');
	//	std::cout<<"newdata: "<<newData<<std::endl;
		if( newData != "" && ( state == STATE_PLAYING || state == STATE_IDLE || state == STATE_SHOWING ) )
		{
			readFromSerial = newData;
		//	std::cout<<"*"<<std::endl;
			// float rawstrength =  pow( fabs( 507 - atof( readFromSerial.c_str()  ) ) , 0.5);
			float rawstrength = atof( readFromSerial.c_str() );
			if( rawstrength>=10)
			{
				if( state == STATE_IDLE || state == STATE_SHOWING )
				{
					best_pull = 0;
					state = STATE_PLAYING;
				}
				did_pull = true;
				ZeroTime();
				strength = ofMap( rawstrength , 0, max_strength, 0/*dortoka degrees*/,100/*dortoka degrees*/);
				
				if( strength > 100.0 ) strength = 100.0;




				if(show_strength>best_pull) 
					best_pull = show_strength;


				std::cout<<" *** pulling "<<strength<<std::endl;
			}
			else
			{
				if( did_pull )
				{
					state = STATE_COMPUTING;
				}
				strength = 0;
			}


		}

	}

	if( CurrTime() > IDLE_T && state == STATE_PLAYING)
	{
		std::cout<<"state = IDLE"<<std::endl;
		state = STATE_IDLE;
				serial.drain();

	}
	
	if( CurrTime() > AFTER_PULL_T && did_pull && state == STATE_COMPUTING )
	{
		std::cout<<"Your pulled about "<<best_pull<<std::endl;
		did_pull = false;

		state = STATE_SHOWING;
				serial.drain();

		ZeroTime();
	}

	if( CurrTime() > SHOW_T && state == STATE_SHOWING)
	{
		state = STATE_IDLE;
		//serial.drain();

	}
	
}

//--------------------------------------------------------------
void ScreenGame::draw(){



	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glDisable(GL_DEPTH_TEST);

	//    ofEnableAlphaBlending();



	ofSetColor(255,255);
		bg.draw(0,0,ofGetWidth(), ofGetHeight());
		leaves_bottom.draw(0,0,ofGetWidth(), ofGetHeight());
		leaves_top.draw(0,0,ofGetWidth(), ofGetHeight());
		ofSetColor(255);



		termometer_bg.draw(610,151);

		int which_temp =  std::ceil( strength / 10.0 );
		if(which_temp<=1) which_temp=0;
		if(which_temp>=temperature.size()) which_temp=temperature.size()-1;
		temperature[which_temp].draw(651, 180);


		podium.draw(871,664);
		bear_bg.draw(885,31);



		ofImage cropped = bear;
		if( strength< 10 ) strength = 0.0;

					//Filter for smoothness
		show_strength += (strength - show_strength)*0.4;
		cropped.crop( 0, bear.getHeight()*(100.0 - show_strength)/100.0, bear.getWidth(), bear.getHeight() * show_strength/100.0 );
		cropped.draw( 885, 31 + bear.getHeight()*(100.0 - show_strength)/100.0);

		if( state == STATE_COMPUTING)
		{
			ofSetColor(255,  (1+sin(CurrTime()/1000.0*200.0)) * 128     );
						font.drawString("computing...", ofGetWidth()*0.05,  ofGetHeight()*0.5 );
				// drawMultilineString( "computing..."	, ofVec2f(ofGetWidth()*0.0,  ofGetHeight()*0.5), 500, font, false, ofColor(255,  (1+sin(CurrTime()/1000.0*400.0)) * 128     ));
		}
		if( state == STATE_SHOWING)
		{
			std::stringstream ss;
			ss<<best_pull<<"%"<<std::endl;
			ofSetColor(255);
			font.drawString("You pulled:",ofGetWidth()*0.05,  ofGetHeight()*0.5 );
			font.drawString(ss.str(),ofGetWidth()*0.05,  ofGetHeight()*0.6 );
			// drawMultilineString( ss.str()	, ofVec2f(ofGetWidth()*0.0,  ofGetHeight()*0.5), 300, font, false, ofColor(255,  255     ));
		}


}

ofImage ScreenGame::cropToPercentage(ofImage src, float pctg)
{
	ofImage img = src;

	src.crop( 0, src.getHeight()*(100.0 - pctg), src.getWidth(), src.getHeight() * pctg );

	return img;
}

void ScreenGame::reset()
{
	state = STATE_PLAYING;
	playWinnerSound = true;
	life = INIT_LIFE;
	points = 0;
	ZeroTime();
	anim.setParameters(1,easingbounce,ofxTween::easeOut, 1200.0, 0.0, 2000.0, 0.0);
	winnerSize.setParameters(2,easingelastic,ofxTween::easeOut, 0.0, 1.0, 2000.0, 1500.0);
}

bool ScreenGame::haveWinner()
{
	//TODO
	return false;
}


