/*
 * ScreenGame.h
 *
 *  Created on: 24/02/2014
 *      Author: xaviersuaucuadros
 */

#ifndef SCREENGAME_H_
#define SCREENGAME_H_

#define NO_NITE

#include "Screens.h"

#include "ofxXmlSettings.h"
#include "ofxTween.h"
#include "ofxSerial.h"


#include <ctime>


const int INIT_LIFE = 3;
const int WIN_POINTS = 4;

const int IDLE_T = 10000;
const int AFTER_PULL_T = 3000;
const int SHOW_T = 5000;

const int STATE_COMPUTING = 10;
const int STATE_SHOWING = 11;
const int STATE_IDLE = 12;


class ScreenGame : public NatScreen{

public:
    void setup();
    void update();
    void draw();
    void load_language_data();
    void reset();

    
    ofImage cropToPercentage(ofImage src, float pctg);

    
    bool haveWinner();

    			/**
    			 * Specific game vars
    			 */

                 ofImage bear_bg, bear, podium;
                 ofImage termometer_bg;
                 std::vector<ofImage> temperature;




    /*Serial vars*/
                 ofxSerial serial;
                 std::string readFromSerial;
                 float strength, max_strength, show_strength, best_pull;
                 bool did_pull;


             };

#endif /* SCREENGAME_H_ */
