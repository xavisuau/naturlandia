**JOC NATURLANDIA**
==============

NUC install instructions
--------
Install Ubuntu 12.04 LTS
Download and run installation script
    wget https://www.dropbox.com/s/ayjysmf83ncf5ds/install_naturlandia_NUC.sh
    sudo ./install_naturlandia_NUC.sh

Setup intructions
--------
Checkout the whole project

    cd ofx_path/apps
    git clone https://xavisuau@bitbucket.org/xavisuau/naturlandia.git
    cd naturlandia

Download the required addons (add more addons in the script if needed)

    ./downloadAddons.sh

Run instructions
--------------

**Run for development**

Go to a game's folder (ie. joc-os-al-mon)

    cd game_folder
Run setEnv.sh

	source setEnv.sh path-that-contains-OPENNI-and-NITE
Compile and run

	make && make run


**Run at startup using SO daemon configuration through upstart (final installation)**

Set the name of the game to be used

	cd naturlandia
	gedit naturlandiaDaemon.conf 
And make a link in the upstart init dir

	cd /etc/init
	sudo ln -s /home/fezoo/openFrameworks/apps/naturlandia/naturlandiaDaemon.conf
	
Check if the game starts after reboot

	sudo reboot -h now
Make sure caffeine will start at startup, and set caffeine's preferences to monitor the current game

Problems with Serial port Ubuntu
--------------------------------

The serial port used by Arduino in Ubuntu is /dev/ttyACM0
Add your user to the dialout group:

	sudo usermod -a -G dialout your_username
and set the permissions for /dev/ttyACM0

	sudo chmod a+rw /dev/ttyACM0
	sudo chmod a+rw /dev/ttyUSB0
This should be all.

If you use the Arduino IDE, the Tools->Serial option is grayed out. To enable it, run with sudo:

	sudo arduino

Problems with sound in Ubuntu
--------------------------------
If you experience the following error:
    
    sfReadFile() couldn't read "some mp3 file"
You will need to install codecs and clean openFrameworks:

	sudo apt-get install libmpg123-dev
	make clean -C /home/einnova/workspace/openFrameworks/libs/openFrameworksCompiled/project/
