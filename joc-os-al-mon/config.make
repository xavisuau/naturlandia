# add custom variables to this file

# OF_ROOT allows to move projects outside apps/* just set this variable to the
# absoulte path to the OF root folder

OF_ROOT = ../../..
OF_ADDONS = .
NATURLANDIA_LIB = ../ofxNaturlandiaLib


# USER_CFLAGS allows to pass custom flags to the compiler
# for example search paths like:
# USER_CFLAGS = -I src/objects

#alopez: In this case the ofxTween addon is failing on linking or on compilation. 
#If I directly refer to the version of OF_ADDONS it does not link; If I add the addon on openFrameworks and I write in addons.make have the same (?).
#I try cloning first the addon from our repository

USER_CFLAGS = -DUSE_HAND  -I$(NATURLANDIA_LIB) -I$(NATURLANDIA_LIB)/Screens -I$(OF_ADDONS)/ofxXmlSettings/libs/ -I$(OF_ADDONS)/ofxXmlSettings/src -I./ofxTween -I./ofxTween/Easings/ -I$(OF_ADDONS)/ofxTrueTypeFontUC/src/ -I$(OPENNI2_INCLUDE) -I$(NITE2_INCLUDE)


# USER_LDFLAGS allows to pass custom flags to the linker
# for example libraries like:
# USER_LDFLAGS = libs/libawesomelib.a

USER_LDFLAGS = -lcurl $(NITE2_REDIST64)/libNiTE2.so $(OPENNI2_REDIST)/libOpenNI2.so


EXCLUDE_FROM_SOURCE="bin,.xcodeproj,obj"

# change this to add different compiler optimizations to your project

USER_COMPILER_OPTIMIZATION = -march=native -mtune=native -Os


# android specific, in case you want to use different optimizations
USER_LIBS_ARM = 
USER_LIBS_ARM7 = 
USER_LIBS_NEON = 

# android optimizations

#ANDROID_COMPILER_OPTIMIZATION = -Os

#NDK_PLATFORM = android-8

# uncomment this for custom application name (if the folder name is different than the application name)
#APPNAME = folderName

# uncomment this for custom package name, must be the same as the java package that contains OFActivity
#PKGNAME = cc.openframeworks.$(APPNAME)





# linux arm flags

LINUX_ARM7_COMPILER_OPTIMIZATIONS = -march=armv7-a -mtune=cortex-a8 -finline-functions -funroll-all-loops  -O3 -funsafe-math-optimizations -mfpu=neon -ftree-vectorize -mfloat-abi=hard -mfpu=vfp



