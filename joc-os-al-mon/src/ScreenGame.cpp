/*
 * ScreenGame.cpp
 *
 *  Created on: 24/02/2014
 *      Author: xaviersuaucuadros
 */


#include "ScreenGame.h"

//--------------------------------------------------------------
void ScreenGame::setup(){

	/*
	 * init common vars
	 */
	NatScreen::initCommonVars();

		life = INIT_LIFE;
		points = 0;
		heart.setLife(life);

		leaves_bottom.loadImage("../../../common-assets/plantes_baix.png");
		leaves_top.loadImage("../../../common-assets/plantes_dalt.png");

	/*
	 * init specific vars
	 */
	currentMap = 0;


	//CustomFonts
	infotitlefont.loadFont("../../../common-assets/verdana.ttf", 50, true, true);
	infotitlefont.setLineHeight(30.0f);
	infotitlefont.setLetterSpacing(1.0);
	infofont.loadFont("../../../common-assets/verdana.ttf", 20, true, true);
	infofont.setLineHeight(30.0f);
	infofont.setLetterSpacing(1.0);
	infotitlefontRUS.loadFont("../../../common-assets/verdana.ttf", 50, true, true);
	infotitlefontRUS.setLineHeight(30.0f);
	infotitlefontRUS.setLetterSpacing(1.0);
	infofontRUS.loadFont("../../../common-assets/verdana.ttf", 20, true, true);
	infofontRUS.setLineHeight(30.0f);
	infofontRUS.setLetterSpacing(1.0);

	quadre.loadImage("../../../common-assets/lang/quadre.png");

	osNames.push_back("oso-pardo");
	osNames.push_back("oso-malayo");
	osNames.push_back("oso-negro-americano");
	osNames.push_back("oso-panda");
	osNames.push_back("oso-andino");
	osNames.push_back("oso-bezudo");
	osNames.push_back("oso-tibetano");
	osNames.push_back("oso-polar");



	osMap.loadImage("game/mapa.png");
	//	mark.loadImage("game/content/boto-gps.png");
	//	mark_wrong.loadImage("game/content/boto-gps-incorrecte.png");
	//	mark_correct.loadImage("game/content/boto-gps-correcte.png");



	osImages = vector<ofImage>(osNames.size());
	float iconWidth = 493;
	float iconHeight = 485;
	bool iconsCentered = false;
	for( int i=0; i<osNames.size(); i++)
	{
		stringstream ss;


		ss.str("") ;
		ss<<"game/content/"<<osNames[i]<<".png";
		cout<<"adding "<<ss.str()<<endl;
		osImages[i].loadImage(ss.str());

		ofVec2f newsize(493, 485); //hardcoded dortoka
		ofVec2f newpos = ofVec2f(136, 308) + newsize*0.5;  //hardcoded dortoka

		cout<<"pushing... "<<flush;

		osIcons.push_back( DragOs( newpos,newsize,osNames[i],&osImages[i], handAdaptor, iconsCentered ) );
		cout<<"done "<<endl;

	}

	//Create questions at positions obtained from xml conf
	ofxXmlSettings settings;
	settings.loadFile("strings.xml");
	for( int i=0; i<osNames.size(); i++)
	{
		int x =  settings.getValue("GAMECONF:" + osNames[i] + ":x", 0 );
		int y =  settings.getValue("GAMECONF:" + osNames[i] + ":y", 0 );
		std::cout<<"Question "<<osNames[i]<<std::endl;
		questions.push_back(  MapQuestion( ofVec2f(x,y), &osIcons[i], osNames[i] )   );
		std::cout<<"Question "<<osNames[i]<<" done"<<std::endl;

	}


	std::random_shuffle(questions.begin(),questions.end());

	load_language_data();

	//
	ofSetVerticalSync(true);
	ofSetFrameRate(30);
	ofBackground(255,255,255);
	std::cout << "end! " << std::endl;


}

void ScreenGame::load_language_data()
{
	loadCommonStrings(language);
	//now load that same file and get the values out
	ofxXmlSettings settings;
	settings.loadFile("strings.xml");
	explanationStr = settings.getValue(language + ":explanation", "Mou la mà per jugar!");
}

int ScreenGame::getClosestIcon( ofVec2f pushPos )
{
	int closest;
	float minDist = 10000;
	for( int i=0; i<questions.size(); i++)
	{
		float dist = pushPos.distance( questions[i].icon->pos + questions[i].icon->siz*0.0 );
		if( dist < minDist )
		{
			minDist = dist;
			closest = i;
		}
	}
	return closest;
}

//--------------------------------------------------------------
void ScreenGame::update(){


//	std::cout<<"FPS: "<<ofGetFrameRate()<<std::endl;
	next_screen = ScreenInfo();

		//Updates the question and explanation buttons
	updateExplanation();
		if( state == STATE_EXPLANATION ) return;


	menuButton.updateSelectProgress();
	if( menuButton.selected )
	{
		menuButton.reset();
		next_screen = ScreenInfo(SCREEN_WELCOME, language);
	}




	if( !haveWinner() && state == STATE_PLAYING && CurrTime() > 1000)
	{

		//Update all quastions, we don't know which one the user wants to pick
		for( int i=0; i<WIN_POINTS; i++)
		{
			//if( i != points ) continue;
			if( questions[i].won || questions[i].failed ) continue;

			questions[ i ].update( questions[points].icon, dyngest->push );
		}

/*
 * 	Firstly, check if the bear is properly placed
 */

		if( questions[ points ].won )
		{
			correctSound.play();
			info.loadInfo( questions[points].name, language );
			info.descriptionLines = cutToStringVec(info.description, 720, infofont);

			bar.setPoints(points+1);
			//Load answer bear info
			state = STATE_GOOD_ANSWER;
			return;
		}


/*
 * 	Secondly, check if the bear is wrongly placed
 */
		for( int i=0; i<WIN_POINTS; i++)
		{

			if( questions[i].won || questions[i].failed ) continue;

			/// Check if failed (icon released outisde the question hotspot)
			if( questions[ i ].selected )
			{
				wrongSound.play();
				questions[ i ].failed = true;
				for( int q=0; q<WIN_POINTS; q++)  questions[ q ].selected = false;

				life--;
				heart.setLife(life);
				if( life == 0 )
				{
					state = STATE_REPLAY;
					return;
				}
				break; // <---- avoids losing more than 1 life if a bear is placed over 2 (or more) wrong marks
			}

			if( questions[i].icon -> HasTag("want_to_release"))
			{
				questions[i].icon->UnTag("want_to_release");
    			questions[i].icon->Tag("grabbed");
    			std::cout<<"grabbing again"<<std::endl;
			}
		}



	}

	/*
	 * Play again after winning or losing, trigger using a swipe
	 */
	if(  (  state != STATE_PLAYING ) &&
			dyngest->push 			&&
			CurrTime() > 200 )
	{
		swipeSound.play();
		ZeroTime();

		if( state == STATE_GOOD_ANSWER )
		{
			state = STATE_PLAYING;
			points++;
			for( int i=0; i<questions.size(); i++)
			{
				if( questions[i].failed )
					questions[i].failed = false;
			}	

			return;
		}
		if( state == STATE_EXPLANATION )
		{
			state = STATE_PLAYING;
			return;
		}
		next_screen = ScreenInfo(SCREEN_WELCOME, language);

		reset();
	}

}


void ScreenGame::reset()
{
	state = STATE_PLAYING;
	playWinnerSound = true;
	currentMap = 0;
	life = INIT_LIFE;
	points = 0;
	bar.setPoints(0);
	heart.setLife(INIT_LIFE);
	ZeroTime();
	for( int i=0; i<questions.size(); i++)
	{
		questions[ i ].reset();
	}
	std::random_shuffle(questions.begin(),questions.end());

	anim.setParameters(1,easingbounce,ofxTween::easeOut, 1200.0, 0.0, 2000.0, 0.0);
	winnerSize.setParameters(2,easingelastic,ofxTween::easeOut, 0.0, 1.0, 2000.0, 1500.0);

}

//--------------------------------------------------------------
void ScreenGame::draw(){



	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glDisable(GL_DEPTH_TEST);


	if( state == STATE_EXPLANATION )
	{
		explanationImg.draw(0,0);
		returnButton.display();
		return;
	}

	ofSetColor(255,255);
	bg.draw(0,0,ofGetWidth(), ofGetHeight());
	leaves_bottom.draw(0,0,ofGetWidth(), ofGetHeight());
	leaves_top.draw(0,0,ofGetWidth(), ofGetHeight());
	ofSetColor(255);

	float Y = anim.update();

	if( state != STATE_GOOD_ANSWER )
	{
		osMap.draw(0, 0 /*- Y*/, ofGetWidth(), ofGetHeight() );


		if( state == STATE_PLAYING )
			questions[points].drawIcon();


		for( int i=0; i<WIN_POINTS; i++)
			questions[ i ].draw(  );
	}



	ofxTrueTypeFontUC * usefont = &font;
	if( language == LANG_RUS)
		usefont = &fontRUS;


	ofSetColor(255);




	if( state == STATE_WINNER )
	{
		float imgSize = winnerSize.update();
		winner_bg.draw(ofGetWidth()/2, ofGetHeight()/2, ofGetWidth()*imgSize, ofGetHeight()*imgSize);
		ofSetColor(0);
//				usefont->drawString( winnerStr, ofGetWidth()*0.3,  ofGetHeight()*0.4  );

				drawMultilineString(winnerStr, ofVec2f(ofGetWidth()*0.5,  ofGetHeight()*0.5), 400, *usefont, true);

				ofSetColor(255);
				winner_icon.draw(ofGetWidth()/2, ofGetHeight()*0.65);


		if( playWinnerSound && !winnerSound.getIsPlaying() )
		{
			winnerSound.play();
			playWinnerSound = false;
		}
	}

	if( state == STATE_REPLAY )
	{
		float replaySize = winnerSize.update();
		replay_bg.draw(ofGetWidth()/2, ofGetHeight()/2, ofGetWidth()*replaySize, ofGetHeight()*replaySize);
		ofSetColor(0);
//		usefont->drawString( replayStr, ofGetWidth()*0.3,  ofGetHeight()*0.4  );
		drawMultilineString(replayStr, ofVec2f(ofGetWidth()*0.5,  ofGetHeight()*0.5), 650, *usefont, true);

		ofSetColor(255);
		replay_icon.draw(ofGetWidth()/2, ofGetHeight()*0.65);
	}




	if( state == STATE_GOOD_ANSWER )
	{
		int X=550;
		int Y=290;
		quadre.draw(0,0);

		ofxTrueTypeFontUC * useinfotitlefont = &infotitlefont;
		if( language == LANG_RUS)
			useinfotitlefont = &infotitlefontRUS;
		ofxTrueTypeFontUC * useinfofont = &infofont;
		if( language == LANG_RUS)
			useinfofont = &infofontRUS;

		useinfotitlefont->drawString( info.name , X, Y-10  );
		useinfofont->drawString( info.latin, X, Y+40  );
		useinfofont->drawString( info.height, X, Y+100  );
		useinfofont->drawString( info.weight, X, Y+140  );
//		useinfofont->drawString( cutStringToWidth(info.description, 720, *useinfofont), X, Y+200  );
		drawStringVec(info.descriptionLines, ofVec2f(X, Y+200), 720, *useinfofont, false, ofColor(255) );
	}

	if( state == STATE_EXPLANATION )
		{
			float scale = 1.0;//anim.update();

			splash.draw(ofGetWidth()*0.5, ofGetHeight()*0.5,ofGetWidth()*scale, ofGetHeight()*scale);
			ofSetColor(255);

			drawMultilineString(explanationStr, ofVec2f(ofGetWidth()*0.5,  ofGetHeight()*0.5), 500, *usefont, true, ofColor(0));

			ofSetColor(255);
		}


	bar.display( ofVec2f(1668,519) );
	heart.display( ofVec2f(1678,225) );
	menuButton.display();
	helpButton.display();
}





bool ScreenGame::haveWinner()
{
	if( points >= WIN_POINTS ) state = STATE_WINNER;
	return points >= WIN_POINTS;
}



