/*
 * ScreenGame.h
 *
 *  Created on: 24/02/2014
 *      Author: xaviersuaucuadros
 */

#ifndef SCREENGAME_H_
#define SCREENGAME_H_

//#include "HandProvider.h"
//#include "HandTrackerThread.h"
#include "Screens.h"

#include "ofxXmlSettings.h"
#include "GUIelements.h"
//#include "question.h"
#include "ofxTween.h"

const int INIT_LIFE = 3;
const int WIN_POINTS = 4;


struct OsInfo
{
	std::string name, latin, weight, height, description;
	std::vector<std::string> descriptionLines;
	OsInfo(){}
	~OsInfo(){}
	void loadInfo(std::string type, std::string lang)
	{
		ofxXmlSettings settings;
		settings.loadFile("strings.xml");
		name 		= settings.getValue("GAMECONF:" + type + ":" + lang + ":name", "default");
		latin 		= settings.getValue("GAMECONF:" + type + ":" + lang + ":latin", "default");
		weight 		= settings.getValue("GAMECONF:" + type + ":" + lang + ":weight", "default");
		height 		= settings.getValue("GAMECONF:" + type + ":" + lang + ":height", "default");
		description = settings.getValue("GAMECONF:" + type + ":" + lang + ":description", "default");
	}
};


class DragOs : public GUIselectablePImage
{
public:
	DragOs() : GUIselectablePImage(){}
	DragOs( ofVec2f & _pos, ofVec2f & _siz, std::string t, ofImage * _pimg, HandPoseAdaptor * _ha, bool _centered = false ) : GUIselectablePImage(_pos, _siz, t, _pimg, _ha, _centered)
	{
		pimg->setAnchorPercent(0.5,0.5);

		initPos = _pos;
		initSize = _siz;
		name = t;
		Tag("idle");
	}

	~DragOs(){}


	bool handOver()
	{
		//cout<<"["<<name<<"] HANDOVER POS : "<<pos<<endl;
		return ( handAdaptor->getHandScaled().x>pos.x-siz.x/2 && handAdaptor->getHandScaled().x<pos.x+siz.x/2 && handAdaptor->getHandScaled().y>pos.y-siz.y/2 && handAdaptor->getHandScaled().y<pos.y+siz.y/2 ) ;
	}

	void update_grab( bool push = false)
	{

		if( ( HasTag("idle") || HasTag("released") ) && handOver() && push )
		{
			siz = initSize * 0.2;
			UnTag("idle");
			UnTag("released");
			Tag("grabbed");
			pos = handAdaptor->getHandScaled() - ofVec2f(0, siz.y)  ;
			drag = pos;//handAdaptor->getHandScaled() ;
			std::cout<<name<<" grabbed"<<std::endl;
			return;
		}

		if( HasTag("grabbed") )
		{
				//Drag image
			pos += ( handAdaptor->getHandScaled() - drag );
			drag = handAdaptor->getHandScaled();

			if( push )
			{
				UnTag("grabbed");
				Tag("want_to_release");
				std::cout<<name<<" want_to_release"<<std::endl;
			}
		}
	}

	void display( ofVec2f delta = ofVec2f(0,0), ofVec2f scale = ofVec2f(1.0,1.0))
	{
		if( handOver() )
		{
			ofSetColor(255,100);
		}
		pimg->draw(pos.x + delta.x, pos.y + delta.y, siz.x * scale.x, siz.y * scale.y);
		ofSetColor(255,255);
	}

	void reset()
	{
		UnTag("grabbed");
		UnTag("released");
		UnTag("want_to_release");
		Tag("idle");
		pos = initPos;
		siz = initSize;
	}

	ofVec2f  drag;
	ofVec2f  initPos, initSize;
	std::string name;
};

class MapQuestion{
    
public:
    MapQuestion(){}
    ~MapQuestion(){}
    MapQuestion( ofVec2f _winpos, DragOs * _icon, std::string _n)
    {
    	name = _n;
        icon = _icon;
        winpos = _winpos;

        won = false;
        failed = false;
    	untouched = true;
        selected = false;

        //HOT Spot size
        W = 200;
        H = 200;

    	mark.loadImage("game/boto-gps.png");
    	mark.setAnchorPercent(0.5,0.5);
    	mark_wrong.loadImage("game/boto-gps-incorrecte.png");
    	mark_wrong.setAnchorPercent(0.5,0.5);
    	mark_correct.loadImage("game/boto-gps-correcte.png");
    	mark_correct.setAnchorPercent(0.5,0.5);

    	std::cout<<"before"<<std::endl;
    	markY.setParameters(1,easingbounce,ofxTween::easeOut, 1200, 0, 2000.0, std::rand()%1000 );
    	std::cout<<"after"<<std::endl;

    }


    
    bool isIconOnZone(DragOs * os)
    {
        ofVec2f iconC = os->pos -os->siz/2;
        return ( iconC.x > winpos.x - W/2 &&
                 iconC.x < winpos.x + W/2 &&
                 iconC.y > winpos.y - H/2 &&
                 iconC.y < winpos.y + H/2 );
    }
    
  
    void update(DragOs * os, bool push = false)
    {


    	os->update_grab(push);
    	if( os->HasTag("want_to_release")  )
    	{

    	if( won ) 
    	{
    			return;
    	}


    		if(  name.compare(os->name) == 0 && isIconOnZone(os) )
    		{
    			won = true;
    			failed = false;
    			os->UnTag("want_to_release");
    			os->Tag("released");
    			    	std::cout<<os->name<<" released on "<<name<<", won"<<std::endl;
    		}
    		else if( name.compare(os->name) != 0 && isIconOnZone(os))
    		{
    			selected = true;
    			won = false;
    			os->UnTag("want_to_release");
    			os->Tag("grabbed");
    			std::cout<<os->name<<" released on "<<name<<", lost"<<std::endl;
    		}
   
    	}
    }

    
void drawIcon()
{

 	if( icon->HasTag("idle"))
     		icon->display();
     else
    	  icon->display( ofVec2f(0,-icon->siz.y*0.3) );

}

    void draw( bool showicon = true )
    {

    	if(!won && !failed)
    	{
 			mark.draw(winpos + ofVec2f(0, -markY.update()));
    	}
    	
    	if(won)
    	{	
    		icon->pos = winpos;
    		icon->display(ofVec2f(24,12) ); // hardcoded dortoka
		}

    	if( won )
    	{
    		ofSetColor(255,120);
    		mark_correct.draw(winpos);
    	}
    	if( failed )
    	{
    		 ofSetColor(255,120);
			 mark_wrong.draw(winpos);
        }
        ofSetColor(255,255);
            
    }
    
    void reset()
    {
        won = false;
        failed = false;
        selected = false;
        icon -> reset();
    	markY.setParameters(1,easingbounce,ofxTween::easeOut, 1200, 0, 2000.0, std::rand()%1000);
    	untouched = true;
    }

    
    ofImage mark, mark_correct, mark_wrong;
    float W,H;
    DragOs * icon;
    ofVec2f initPos, initSize, winpos;
    bool won, failed, untouched, selected;
    std::string name;

	ofxEasingBounce      easingbounce;
	ofxTween markY;
};


class ScreenGame : public NatScreen{

public:

	ScreenGame() : NatScreen(){}
	~ScreenGame(){}

	void setup();
	void update();
	void draw();
	void load_language_data();
	void reset();


	int getClosestIcon( ofVec2f pushPos );

	bool haveWinner();
	std::string cutStringIntoLines(std::string str);

	void exit();




	/**
	 * Specific game vars
	 */
	ofxTrueTypeFontUC	infofont, infotitlefont;
	ofxTrueTypeFontUC	infofontRUS, infotitlefontRUS;

	ofImage osMap, mark, mark_wrong, mark_correct, quadre;
	std::vector<ofImage> osZones;

	ofImage zoneOs;
	std::vector<ofImage> osImages;
	std::vector<DragOs> osIcons;

	std::vector<MapQuestion> questions;
	std::vector<std::string> osNames;
	int currentMap;

	std::vector<std::string> explanationLines;

	OsInfo info;
};

#endif /* SCREENGAME_H_ */
