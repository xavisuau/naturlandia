/*
 * ScreenGame.cpp
 *
 *  Created on: 24/02/2014
 *      Author: xaviersuaucuadros
 */


#include "ScreenGame.h"


//--------------------------------------------------------------
void ScreenGame::setup(){


	/*
	 * init common vars
	 */
	NatScreen::initCommonVars();

	life = INIT_LIFE;
	points = 0;
	heart.setLife(life);

	leaves_bottom.loadImage("../../../common-assets/plantes_baix.png");
	leaves_top.loadImage("../../../common-assets/plantes_dalt.png");

	/*
	 * init specific vars
	 */

	 currentQuestion = 0;

	 	for( int i=0; i<lang_vec.size(); i++)
		{
			yesButton[ lang_vec[i] ] =  GUIselectableImage(0, 0, "YES_"+lang_vec[i],"game/boto si_hover_"+lang_vec[i]+".png", handAdaptor, true) ;
			yesButton[ lang_vec[i] ].setHoverImage("game/boto si_"+lang_vec[i]+".png");
			yesButton[ lang_vec[i] ].setPos(520,560);

			noButton[ lang_vec[i] ] =  GUIselectableImage(0, 0, "NO_"+lang_vec[i],"game/boto no_hover_"+lang_vec[i]+".png", handAdaptor, true) ;
			noButton[ lang_vec[i] ].setHoverImage("game/boto no_"+lang_vec[i]+".png");
			noButton[ lang_vec[i] ].setPos(965,560);
		}



	quadre.loadImage("game/quadre.png");

	//Fonts
	fontquestion.loadFont("../../../common-assets/verdana.ttf", 35, true, true);
	fontquestion.setLineHeight(50.0f);
	fontquestion.setLetterSpacing(1.0);
	fontquestionRUS.loadFont("../../../common-assets/verdana.ttf", 35, true, true);
	fontquestionRUS.setLineHeight(50.0f);
	fontquestionRUS.setLetterSpacing(1.0);
	loadQuestions();

	ZeroTime();

	//
	ofSetVerticalSync(true);
	ofSetFrameRate(60);
	ofBackground(255,255,255);

}

void ScreenGame::loadQuestions()
{
	// Load a CSV File.
	csv = ofxCsv(); // <--- a bug in ofxCsv, loadFile does not erase, it appends...
	csv.loadFile(ofToDataPath("game/questions/" + language + ".csv"),";");
	std::cout<<"oooooo Loaded "<<csv.data.size()<<" questions in "<<language<<std::endl;

	questions.clear();
	for (int i=0; i<csv.data.size(); i++) {
		//	        cout << "    "<<i<<" : "<<csv.data[i][1] << endl;
		questions.push_back( BoolQuestion( csv.data[i][1], csv.data[i][2] == "X") );//, &noAnswer, 0) );
	}

	// using built-in random generator to Shuffle Questions
	std::random_shuffle ( questions.begin(),  questions.end() );
}


void ScreenGame::load_language_data()
{
	loadCommonStrings(language);

	//now load that same file and get the values out
	ofxXmlSettings settings;
	settings.loadFile("strings.xml");
	explanationStr = settings.getValue(language + ":explanation", "Mou la mà per jugar!");

	loadQuestions();

}

//--------------------------------------------------------------
void ScreenGame::update(){


	next_screen = ScreenInfo();


	//Updates the question and explanation buttons
	updateExplanation();
		if( state == STATE_EXPLANATION ) return;


	menuButton.updateSelectProgress();
	if( menuButton.selected )
	{
		menuButton.reset();
		next_screen = ScreenInfo(SCREEN_WELCOME, language);
	}




	if( !haveWinner() && state == STATE_PLAYING && CurrTime() > 2000 )
	{

		//yesButton.updateSelectProgress();
		//noButton.updateSelectProgress();

				yesButton[language].updateWithPush(dyngest->push);
				noButton[language].updateWithPush(dyngest->push);
		/*
		 * CORRECT ANSWER
		 */
		if( (yesButton[language].selected && questions[currentQuestion].answer == true) ||
				(noButton[language].selected && questions[currentQuestion].answer == false) 	)
		{
			correctSound.play();
			points++;
			bar.setPoints(points);
			yesButton[language].reset();
			noButton[language].reset();
			currentQuestion++;
			ZeroTime();
		}
		/*
		 * WRONG ANSWER
		 */
		if( (yesButton[language].selected && questions[currentQuestion].answer == false) ||
				(noButton[language].selected && questions[currentQuestion].answer == true) 	)
		{
			wrongSound.play();
			life--;
			heart.setLife(life);
			yesButton[language].reset();
			noButton[language].reset();
			currentQuestion++;
			ZeroTime();

			if( life == 0 )
			{
				state = STATE_REPLAY;
				return;
			}
		}

	}
	else
	{
		yesButton[language].reset();
		noButton[language].reset();
	}

	/*
	 * Play again after winning or losing, trigger using a swipe
	 */
	if(  (  state != STATE_PLAYING ) &&
			dyngest->push 			&&
			CurrTime() > 200 )
	{
		if( state == STATE_EXPLANATION )
		{
			state = STATE_PLAYING;
			return;
		}
		ZeroTime();
		next_screen = ScreenInfo(SCREEN_WELCOME, language);

		reset();
	}



}



void ScreenGame::reset()
{
	state = STATE_PLAYING;
	playWinnerSound = true;
	life = INIT_LIFE;
	points = 0;
	currentQuestion=0;
	bar.setPoints(0);
	heart.setLife(INIT_LIFE);
	yesButton[language].reset();
	noButton[language].reset();
	std::random_shuffle ( questions.begin(),  questions.end() );
	ZeroTime();

	anim.setParameters(1,easingbounce,ofxTween::easeOut, 1200.0, 0.0, 2000.0, 0.0);
	winnerSize.setParameters(2,easingelastic,ofxTween::easeOut, 0.0, 1.0, 2000.0, 1500.0);
}


//--------------------------------------------------------------
void ScreenGame::draw(){



	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glDisable(GL_DEPTH_TEST);

	if( state == STATE_EXPLANATION )
	{
		explanationImg.draw(0,0);
		returnButton.display();
		return;
	}

	
	ofSetColor(255,255);
	bg.draw(0,0,ofGetWidth(), ofGetHeight());
	leaves_bottom.draw(0,0,ofGetWidth(), ofGetHeight());
	leaves_top.draw(0,0,ofGetWidth(), ofGetHeight());
	ofSetColor(255);

	float Y = anim.update();

	quadre.draw(0,0-Y);


	ofxTrueTypeFontUC * usefont = &font;
	if( language == LANG_RUS)
		usefont = &fontRUS;


	ofxTrueTypeFontUC * usefontquestion = &fontquestion;
	if( language == LANG_RUS)
		usefontquestion = &fontquestionRUS;

	//	usefontquestion->drawString( cutStringToWidth(questions[currentQuestion].text, 400, *usefontquestion), 550, 350 - Y);
	drawMultilineString(questions[currentQuestion].text, ofVec2f(ofGetWidth()*0.5,  ofGetHeight()*0.4 - Y), 650, *usefontquestion, true, ofColor(255));

//	if( yesButton.handOver()) 	ofSetColor(255,100);
//	else 						ofSetColor(255,255);
	yesButton[language].display( ofVec2f(0,-Y) );

//	if( noButton.handOver()) 	ofSetColor(255,100);
//	else 						ofSetColor(255,255);
	noButton[language].display( ofVec2f(0,-Y) );


	if( state == STATE_WINNER )
	{
		float imgSize = winnerSize.update();
		winner_bg.draw(ofGetWidth()/2, ofGetHeight()/2, ofGetWidth()*imgSize, ofGetHeight()*imgSize);
		ofSetColor(0);
//				usefont->drawString( winnerStr, ofGetWidth()*0.3,  ofGetHeight()*0.4  );

				drawMultilineString(winnerStr, ofVec2f(ofGetWidth()*0.5,  ofGetHeight()*0.5), 400, *usefont, true);

				ofSetColor(255);
				winner_icon.draw(ofGetWidth()/2, ofGetHeight()*0.65);


		if( playWinnerSound && !winnerSound.getIsPlaying() )
		{
			winnerSound.play();
			playWinnerSound = false;
		}
	}

	if( state == STATE_REPLAY )
	{
		float replaySize = winnerSize.update();
		replay_bg.draw(ofGetWidth()/2, ofGetHeight()/2, ofGetWidth()*replaySize, ofGetHeight()*replaySize);
		ofSetColor(0);
//		usefont->drawString( replayStr, ofGetWidth()*0.3,  ofGetHeight()*0.4  );
		drawMultilineString(replayStr, ofVec2f(ofGetWidth()*0.5,  ofGetHeight()*0.5), 650, *usefont, true);

		ofSetColor(255);
		replay_icon.draw(ofGetWidth()/2, ofGetHeight()*0.65);
	}


	if( state == STATE_EXPLANATION )
	{
		float scale = 1.0;//anim.update();

		splash.draw(ofGetWidth()*0.5, ofGetHeight()*0.5,ofGetWidth()*scale, ofGetHeight()*scale);
		ofSetColor(255);
		std::cout<<explanationStr<<std::endl;

		drawMultilineString(explanationStr, ofVec2f(ofGetWidth()*0.5,  ofGetHeight()*0.5), 500, *usefont, true, ofColor(0));

		ofSetColor(255);
	}

	bar.display( ofVec2f(1668,519) );
	heart.display( ofVec2f(1678,225) );
	menuButton.display();
	helpButton.display();
}


bool ScreenGame::haveWinner()
{

	if( points >= WIN_POINTS ) state = STATE_WINNER;
	return points >= WIN_POINTS;
}






std::vector<std::string> ScreenGame::getNextLineAndSplitIntoTokens(std::istream& str)
{
	std::vector<std::string>   result;
	std::string                line;
	std::getline(str,line);

	std::stringstream          lineStream(line);
	std::string                cell;

	while(std::getline(lineStream,cell,';'))
	{
		result.push_back(cell);
	}
	return result;
}

