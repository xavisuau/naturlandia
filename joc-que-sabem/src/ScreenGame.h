/*
 * ScreenGame.h
 *
 *  Created on: 24/02/2014
 *      Author: xaviersuaucuadros
 */

#ifndef SCREENGAME_H_
#define SCREENGAME_H_

#include "Screens.h"

#include "ofxXmlSettings.h"
#include "ofxCsv.h"
#include "question.h"

using namespace wng;

const int INIT_LIFE = 3;
const int WIN_POINTS = 4;


struct BoolQuestion{

BoolQuestion(std::string _t, bool _a)
{
		text = _t;
		answer = _a;
}

	bool answer;
	std::string text;
};



class ScreenGame : public NatScreen{

public:

	ScreenGame() : NatScreen(){}
    ~ScreenGame(){}

	  void setup();
	    void update();
	    void draw();
		void load_language_data();
		void reset();



	    std::vector<std::string> getNextLineAndSplitIntoTokens(std::istream& str);

	    void loadQuestions();
	    bool haveWinner();


		void exit();

		/**
		 * Specific game vars
		 */
	    ofxCsv csv;

	    std::map<std::string,GUIselectableImage> yesButton, noButton;
	    ofImage quadre;

	    std::vector<BoolQuestion> questions;
	    int currentQuestion;

	    /* Declare Fonts */
	    ofxTrueTypeFontUC	fontquestion, fontquestionRUS;


};

#endif /* SCREENGAME_H_ */
