//#include "HandProvider.h"

void HandProvider::init()
{
	niteRc = nite::NiTE::initialize();
	if (niteRc != nite::STATUS_OK)
	{
		printf("Hand Tracker initialization failed\n");
		exit(1);
	}

	niteRc = handTracker.create();
	if (niteRc != nite::STATUS_OK)
	{
		printf("Couldn't create user tracker\n");
		exit(3);
	}

	handTracker.startGestureDetection(nite::GESTURE_WAVE);
	handTracker.startGestureDetection(nite::GESTURE_CLICK);
	handTracker.setSmoothingFactor(0.055);
	//handTracker.startGestureDetection(nite::GESTURE_HAND_RAISE);
	printf("\nWave or click to start tracking your hand...\n");
}


void HandProvider::process()
{
	niteRc = handTracker.readFrame(&handTrackerFrame);
	if (niteRc != nite::STATUS_OK)
	{
		printf("Get next frame failed\n");
		return;
	}

	const nite::Array<nite::GestureData>& gestures = handTrackerFrame.getGestures();
	for (int i = 0; i < gestures.getSize(); ++i)
	{
		if (gestures[i].isComplete())
		{
			nite::HandId newId;
			handTracker.startHandTracking(gestures[i].getCurrentPosition(), &newId);
		}
	}

	const nite::Array<nite::HandData>& hands = handTrackerFrame.getHands();
	for (int i = 0; i < hands.getSize(); ++i)
	{
		const nite::HandData& hand = hands[i];
		if (hand.isTracking())
		{
			//printf("%d. (%5.2f, %5.2f, %5.2f)\n", hand.getId(), hand.getPosition().x, hand.getPosition().y, hand.getPosition().z);
		}
	}
}


int  HandProvider::getNumHands() const
{
	const nite::Array<nite::HandData>& hands = handTrackerFrame.getHands();
	return hands.getSize();
}

short int  HandProvider::getHandId( int i )
{
	const nite::Array<nite::HandData>& hands = handTrackerFrame.getHands();

	const nite::HandData& hand = hands[i];
	if (hand.isTracking()) return hand.getId();
	else return -1;
}

double  HandProvider::getHandU( int i )
{
	const nite::Array<nite::HandData>& hands = handTrackerFrame.getHands();

	const nite::HandData& hand = hands[i];
	if (hand.isTracking())
	{
		float u, v;
		openni::DepthPixel depth;

		niteRc = handTracker.convertHandCoordinatesToDepth 	( hand.getPosition().x,
				hand.getPosition().y,
				hand.getPosition().z,
				&u,
				&v
			);
		return u;
	}

	else return -100000;
}

double  HandProvider::getHandV( int i )
{
	const nite::Array<nite::HandData>& hands = handTrackerFrame.getHands();

	const nite::HandData& hand = hands[i];
	if (hand.isTracking())
	{
		float u, v;
		openni::DepthPixel depth;

		niteRc = handTracker.convertHandCoordinatesToDepth 	( hand.getPosition().x,
				hand.getPosition().y,
				hand.getPosition().z,
				&u,
				&v
			);
		return v;
	}

	else return -100000;
}

double  HandProvider::getHandZ( int i )
{
	const nite::Array<nite::HandData>& hands = handTrackerFrame.getHands();
    
	const nite::HandData& hand = hands[i];
	if (hand.isTracking()) return hand.getPosition().z;
	else return -100000;
}

double  HandProvider::getHandY( int i )
{
	const nite::Array<nite::HandData>& hands = handTrackerFrame.getHands();
    
	const nite::HandData& hand = hands[i];
	if (hand.isTracking()) return hand.getPosition().y;
	else return -100000;
}

double  HandProvider::getHandX( int i )
{
	const nite::Array<nite::HandData>& hands = handTrackerFrame.getHands();

	const nite::HandData& hand = hands[i];
	if (hand.isTracking()) return hand.getPosition().x;
	else return -100000;
}

void HandProvider::stop()
{

	//handTracker.destroy();
	//handTrackerFrame.release();
	nite::NiTE::shutdown();
	openni::OpenNI::shutdown();

}
