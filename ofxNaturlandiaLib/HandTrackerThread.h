//
//  HandTrackerThread.h
//  jocNaturlandia
//
//  Created by xavier suau on 6/11/13.
//
//
//
//   Thread for hand tracking 
//
//
//
//





#ifndef fezoo_HandTrackerThread_h
#define fezoo_HandTrackerThread_h


#include "HandProvider.h"
#include "HandPoseAdaptor.h"


class HandTrackerThread : public ofThread {
    
public:
    
    //HandTrackerThread() : ofThread(){}
    
    ~HandTrackerThread()
    {
        std::cout<<" --- destroying thread"<<std::endl;
        if (isThreadRunning()) stopThread();
    }
    
    void start()
    {
        startThread(false, true); //non-blocking, verbose
    }
    
    void stop()
    {
        stopThread();
    }

    void init( HandProvider * _tracker, HandPoseAdaptor * _hpa)
    {
        tracker = _tracker;
        hpa     = _hpa;
        filterK = 0.6;
    }
    
    // the thread function
    void threadedFunction() {
        
        // start
        while(isThreadRunning()) {
            
            tracker->process();
  //          float filterK = 0.4;
            hpa->update();//Hand( tracker, filterK );
        }


        
        // done
    }
    HandProvider * tracker;
    HandPoseAdaptor * hpa;
    float filterK;
};



#endif
