//
//  GUIelements.h
//  jocNaturlandia
//
//  Created by Xavier Suau on 15/10/13.
//
//
//
//  Support classes for visualization of GUI elements (images, etc.)
//
//
//
//


#ifndef fezoo_GUIelements_h
#define fezoo_GUIelements_h



#include "HandPoseAdaptor.h"
//#define USE_HAND

#include "ofMain.h"
#include "ofEvents.h"
//#include "ofxTween.h"


const int SELECT_TIMEOUT = 1000;
const int GRAB_TIMEOUT = 1000;
const int RELEASE_TIMEOUT = 1000;
const int RESELECT_TIMEOUT = 1000;

const int GUARDA_MOVING_HAND = 1500;

/*
 * Abstract class defining a selectable GUI item. Must be implemented
 */
class GUIselectable
{
public:

	GUIselectable(){};

	virtual ~GUIselectable(){};



	GUIselectable(float x, float y, std::string t,  HandPoseAdaptor * _ha)
	:name(t)
	{
		pos = ofVec2f(x,y);
		//cout<<"SETTING BASE POS : "<<pos<<endl;
		siz = ofVec2f(100,100);
		timeout = -1;
		selected = false;
		justReleased = false;
		handAdaptor = _ha;
		progress_angle = -1;

	};

	GUIselectable(ofVec2f _pos, ofVec2f _siz, std::string t,  HandPoseAdaptor * _ha)
	:name(t)
	{
		pos = _pos;
		cout<<"SETTING BASE POS : "<<pos<<endl;
		siz = _siz;
		timeout = -1;
		selected = false;
		justReleased = false;
		handAdaptor = _ha;
		progress_angle = -1;
		scale = ofVec2f(1.0,1.0);
	};

	void setPos(float x, float y)
	{
		//std::cout<<"SetPos : "<<x<<"  "<<y<<std::endl;
		pos = ofVec2f(x,y);
	}

	void setSize(float sx, float sy)
	{
		siz = ofVec2f(sx,sy);
	}

	virtual void display(){}; //<-- makes class abstract


	//------TAGS AND TIMERS
	void Tag(std::string tag)
	{
		if( !tags.count(tag) )
		{
			tags[tag] = 1;
		}
	}

	void UnTag(std::string tag)
	{
		if( tags.count(tag) )
		{
			tags.erase(tag);
		}
	}

	bool HasTag(std::string tag)
	{
		return tags.count(tag);
	}

	long int CurrTime()
	{
		return  ofGetElapsedTimeMillis() - time;
	}

	void ZeroTime()
	{
		time = ofGetElapsedTimeMillis();
	}

	//------PROGRESS CIRCLE UPDATE : Turns on "selected" flag. It should be turned off by the user, using reset()
	bool updateSelectProgress()
	{
		if( handAdaptor->numHands() == 0 )
		{
			reset();
			return false;
		}

		if( !HasTag("inside") && handOver() )
		{
			Tag("inside");
			Tag("entered");
			UnTag("exited");
			handEnters();
			//DO THINGS
			timeout = ofGetElapsedTimeMillis();
		}
		if( HasTag("inside") && !handOver() )
		{
			UnTag("inside");
			Tag("exited");
			UnTag("entered");
			handExits();
			//DO THINGS
			timeout = -1;
			progress_angle = -1;
		}

//		if( justReleased ) justReleased = false; //not used

		if( !selected )
		{
			if( HasTag("inside") )
			{
				progress_angle = ofMap( ofGetElapsedTimeMillis() - timeout, 0, SELECT_TIMEOUT, 0, 360 ) + 0.1;

				if(ofGetElapsedTimeMillis() - timeout > SELECT_TIMEOUT)
				{
					selected = true;
				}
			}
		}

//		if( selected && HasTag("exited") )//!handOver())
//		{
//			justReleased = true;
//		}

		return selected;

	}

	virtual void reset()
	{
		selected = false;
		//justReleased = false;
		timeout = ofGetElapsedTimeMillis();
		progress_angle = -1;
	}

	virtual bool handOver()
	{
		//cout<<"["<<name<<"] HANDOVER POS : "<<pos<<endl;
		return ( handAdaptor->getHandScaled().x>pos.x && handAdaptor->getHandScaled().x<pos.x+siz.x && handAdaptor->getHandScaled().y>pos.y && handAdaptor->getHandScaled().y<pos.y+siz.y ) ;
	}

	void handEnters()
	{
		//Do Things
	}

	void handExits()
	{
		//Do other things
	}

	bool updateWithPush(bool push)
	{
		if( handAdaptor->numHands() == 0 )
		{
			reset();
			return false;
		}

		if( justReleased ) justReleased = false;

		//cout<<"["<<name<<"] HANDOVER POS : "<<pos<<endl;
		//		if( !HasTag("inside") && handOver() )
		//		{
		//			Tag("inside");
		//			handEnters();
		//		}
		//		if( HasTag("inside") && !handOver() )
		//		{
		//			UnTag("inside");
		//			handExits();
		//		}

		if( push && handOver()  )
		{
			//			std::cout<<"PUSHED "<<name<<" !"<<endl;
			//			cout<<"HAND IS AT : "<<handAdaptor->getHandScaled()<<endl;
			//			cout<<"POS : "<<pos<<endl;
			//			cout<<"SIZ : "<<siz<<endl;
			if( handOver() )
			{
				selected = true;
				std::cout<<"SELECTED "<<name<<" !"<<endl;
			}
		}
		return selected;
	}






	ofVec2f pos, siz;
	bool  selected, justReleased;
	float timeout, progress_angle;
	std::string name;

protected:
	HandPoseAdaptor * handAdaptor;
	long int time;
	std::map<std::string, int> tags;
	ofVec2f scale;

};

/*
 * Abstract class defining a draggable GUI item. Must be implemented
 */
class GUIdraggable : public GUIselectable
{
public:

	GUIdraggable(){};

	virtual ~GUIdraggable(){};



	GUIdraggable(float x, float y, std::string t,  HandPoseAdaptor * _ha) :  GUIselectable(x,y,t,_ha)
	{
		pressed = false;
		drag = ofVec2f(0,0);
		siz = ofVec2f(100,100);
		timeout = -1;
		selected = false;
		justReleased = false;
		releaseCount = -1;
		name = t;
		handMoving = false;
		lastHand = _ha->getHandScaled();
	};


	virtual void display(){}; //<-- makes class abstract


	//------PROGRESS CIRCLE UPDATE and DRAW
	bool updateSelectProgress()
	{
		if( justReleased ) justReleased = false;

		if( !selected )
		{

			if(  handOver() && timeout < 0) timeout = ofGetElapsedTimeMillis();
			if( !handOver() && timeout >= 0)
			{
				timeout = -1;
				progress_angle = -1;
			}


			if( timeout >= 0 )
			{
				progress_angle = ofMap( ofGetElapsedTimeMillis() - timeout, 0, SELECT_TIMEOUT, 0, 360 ) + 0.1;
				if(ofGetElapsedTimeMillis() - timeout > GRAB_TIMEOUT)
				{
					selected = true;
					releaseCount = ofGetElapsedTimeMillis();
					drag = handAdaptor->getHandScaled();
				}
			}
		}

		if( lastHand.distance( handAdaptor->getHandScaled() ) < 10 )
		{
			handMoving = false;
		}
		else
		{
			handMoving = true;
			releaseCount = ofGetElapsedTimeMillis();
		}
		lastHand = handAdaptor->getHandScaled();
		if( selected )
		{
			//            releaseCount+=4;
			//Drag image
			pos += ( handAdaptor->getHandScaled() - drag );
			drag = handAdaptor->getHandScaled();

			if( ofGetElapsedTimeMillis() - releaseCount > RELEASE_TIMEOUT )
			{
				progress_angle = ofMap( ofGetElapsedTimeMillis() - releaseCount, RELEASE_TIMEOUT, RELEASE_TIMEOUT + SELECT_TIMEOUT, 0, 360 ) + 0.1;

				if(ofGetElapsedTimeMillis() - releaseCount >= RELEASE_TIMEOUT + SELECT_TIMEOUT)
				{
					selected = false;
					releaseCount = ofGetElapsedTimeMillis();
					timeout = ofGetElapsedTimeMillis();
					justReleased = true;
				}
			}
		}

		return selected;
	}


	ofVec2f  drag, lastHand;
	bool  pressed, selected, released, justReleased, handMoving;
	float timeout, releaseCount;
	std::string name;

};

/*
 * Abstract class defining a draggable GUI item. Must be implemented
 */
class GUIselectableDraggable : public GUIselectable
{
public:

	GUIselectableDraggable() : GUIselectable(){};

	virtual ~GUIselectableDraggable(){};



	GUIselectableDraggable(float x, float y, std::string t,  HandPoseAdaptor * _ha) :  GUIselectable(x,y,t,_ha)
	{
		pressed = false;
		drag = ofVec2f(0,0);
		siz = ofVec2f(100,100);
		timeout = -1;
		selected = false;
		justReleased = false;
		releaseCount = -1;
		name = t;
		handMoving = false;
		lastHand = _ha->getHandScaled();
	};

	GUIselectableDraggable(ofVec2f _pos, ofVec2f _siz, std::string t,  HandPoseAdaptor * _ha)
	:GUIselectable(_pos.x,_pos.y,t,_ha)
	{
		pos = _pos;
		siz = _siz;
		timeout = -1;
		selected = false;
		justReleased = false;
		releaseCount = -1;
		name = t;
		handMoving = false;

		lastHand = _ha->getHandScaled();
		reSelectTo = ofGetElapsedTimeMillis();

	};


	virtual void display(){}; //<-- makes class abstract


	//------PROGRESS CIRCLE UPDATE and DRAW
	bool updateSelectProgress()
	{
			if( justReleased )
		{
				justReleased = false;
		}

		if( ofGetElapsedTimeMillis() - reSelectTo < RESELECT_TIMEOUT )
		{
			progress_angle = -1;
			releaseCount = ofGetElapsedTimeMillis();
								timeout = ofGetElapsedTimeMillis();
				return false;
		}



		progress_angle = -1;

		if( !selected )
		{

			if(  handOver() && timeout < 0) timeout = ofGetElapsedTimeMillis();
			if( !handOver() && timeout >= 0)
			{
				timeout = -1;
				progress_angle = -1;
			}


			if( timeout >= 0 )
			{
				progress_angle = ofMap( ofGetElapsedTimeMillis() - timeout, 0,SELECT_TIMEOUT, 0, 360 ) + 0.1;



				if(ofGetElapsedTimeMillis() - timeout > GRAB_TIMEOUT)
				{
					selected = true;
					releaseCount = ofGetElapsedTimeMillis();
					drag = handAdaptor->getHandScaled();
				}

			}

		}

		if( lastHand.distance( handAdaptor->getHandScaled() ) < 10 )
		{
			handMoving = false;
		}
		else
		{
			handMoving = true;
			releaseCount = ofGetElapsedTimeMillis();
		}
		lastHand = handAdaptor->getHandScaled();
		if( selected )
		{
			//            releaseCount+=4;
			//Drag image
			pos += ( handAdaptor->getHandScaled() - drag );
			drag = handAdaptor->getHandScaled();

			if( ofGetElapsedTimeMillis() - releaseCount > RELEASE_TIMEOUT )
			{

				progress_angle = ofMap( ofGetElapsedTimeMillis() - releaseCount, RELEASE_TIMEOUT, RELEASE_TIMEOUT + SELECT_TIMEOUT, 0, 360 ) + 0.1;

				if(ofGetElapsedTimeMillis() - releaseCount >= RELEASE_TIMEOUT + SELECT_TIMEOUT)
				{
					selected = false;
					releaseCount = ofGetElapsedTimeMillis();
					timeout = ofGetElapsedTimeMillis();
					justReleased = true;
					reSelectTo = ofGetElapsedTimeMillis();

				}
			}

		}

		return selected;

	}

	bool updateWithPush(bool push)
	{


		if (push && handOver()) {
			selected = !selected;
			if(selected){
				drag = handAdaptor->getHandScaled();
			}
			else
			{
				selected = false;

				justReleased = true;
			}
		}

		if( selected )
		{
			//            releaseCount+=4;
			//Drag image
			pos += ( handAdaptor->getHandScaled() - drag );
			drag = handAdaptor->getHandScaled();

		}


	}


	ofVec2f  drag, lastHand;
	bool  pressed, selected, released, handMoving;
	float timeout, releaseCount;
	std::string name;
	long reSelectTo;

};






/*
 * class used to store position and text values for a default text element
 */
class GUIselectableImage : public GUIselectable
{
public:

	GUIselectableImage(){}

	virtual ~GUIselectableImage(){}



	GUIselectableImage(float x, float y, std::string t, std::string imstr, HandPoseAdaptor * _ha, bool centered = false) : GUIselectable(x,y,t,_ha)
	{
		img.loadImage(imstr);
		siz = ofVec2f( img.getWidth(), img.getHeight() );
		if(centered)
		{
			pos = pos - siz/2;
		}
		correct = false;
	}

	void setImage( ofImage & _img )
	{
		img = _img;
	}


	void setHoverImage(std::string hover_img_name)
	{
		hover_img.loadImage(hover_img_name);
	}


	void setSize( float sx, float sy )
	{
		siz = ofVec2f(sx,sy);
	}



	void display( ofVec2f delta = ofVec2f(0,0))
	{
		if( hover_img.isAllocated() && handOver() )
		{
			hover_img.draw(pos.x + delta.x, pos.y + delta.y, siz.x, siz.y);
		}
		else
		{
			img.draw(pos.x + delta.x, pos.y + delta.y, siz.x, siz.y);
		}
		//If needed, update select progress
		// updateSelectProgress();
		//        updateWithPush();

		ofSetColor(255,255);

		if( progress_angle > 0.1 )
		{
			//DRAW PROGRESS CIRCLE
			ofPath path = ofPath();
			path.setFillColor(ofColor(255,255,255,180));

			ofPoint pt(handAdaptor->getHandScaled().x, handAdaptor->getHandScaled().y);

			path.setCircleResolution(100);
			path.arc(pt, 50,50, 0, progress_angle, true);
			path.draw();
		}


	}

	ofImage img, hover_img;
	bool correct;


};

class GUIselectablePImage : public GUIselectable
{
public:

	GUIselectablePImage() : GUIselectable() {}

	virtual ~GUIselectablePImage(){}


	GUIselectablePImage(float x, float y, std::string t, ofImage * _pimg, HandPoseAdaptor * _ha, bool _centered = false) : GUIselectable(x,y,t,_ha)
	{
		pimg = _pimg;
		siz = ofVec2f( pimg->getWidth(), pimg->getHeight() );
		centered = _centered;
		if(centered)
		{
			pos = pos - siz/2;
		}
		correct = false;
	}

	GUIselectablePImage(ofVec2f & _pos, ofVec2f & _siz, std::string t, ofImage * _pimg, HandPoseAdaptor * _ha, bool _centered = false) : GUIselectable(_pos,_siz,t,_ha)
	{
		pimg = _pimg;
		siz = _siz;
		centered = _centered;
		if(centered)
		{
			pos = _pos - _siz/2;
		}
		correct = false;
	}


	void setPImage( ofImage * _pimg )
	{
		pimg = _pimg;
	}

	void setPosSize(ofVec2f _pos, ofVec2f _siz)
	{
		siz = _siz;
		if(centered)
		{
			pos = _pos - siz/2;
		}
		else
		{
			pos = _pos;
		}
	}



	void display( ofVec2f delta = ofVec2f(0,0))
	{
		pimg->draw(pos.x + delta.x, pos.y + delta.y, siz.x, siz.y);
	}

	ofImage * pimg;
	bool correct, centered;

};


class GUIselectableDraggablePImage : public GUIselectableDraggable
{
public:

	GUIselectableDraggablePImage(){}

	~GUIselectableDraggablePImage(){}


	GUIselectableDraggablePImage(float x, float y, std::string t, ofImage * _pimg, HandPoseAdaptor * _ha, bool _centered = false) : GUIselectableDraggable(x,y,t,_ha)
	{
		pimg = _pimg;
		siz = ofVec2f( pimg->getWidth(), pimg->getHeight() );
		centered = _centered;
		if(centered)
		{
			pos = pos - siz/2;
		}
		correct = false;
	}

	GUIselectableDraggablePImage(ofVec2f & _pos, ofVec2f & _siz, std::string t, ofImage * _pimg, HandPoseAdaptor * _ha, bool _centered = false) : GUIselectableDraggable(_pos,_siz,t,_ha)
	{
		pimg = _pimg;
		siz = _siz;
		centered = _centered;
		if(centered)
		{
			pos = _pos - _siz/2;
		}
		correct = false;
	}


	void setPImage( ofImage * _pimg )
	{
		pimg = _pimg;
	}

	void setPosSize(ofVec2f _pos, ofVec2f _siz)
	{
		siz = _siz;
		if(centered)
		{
			pos = _pos - siz/2;
		}
		else
		{
			pos = _pos;
		}

	}

	virtual void display( ofVec2f delta = ofVec2f(0,0), ofVec2f scale = ofVec2f(1.0,1.0))
	{
		if( handOver() )
		{
			ofSetColor(255,100);
		}
		pimg->draw(pos.x + delta.x, pos.y + delta.y, siz.x * scale.x, siz.y * scale.y);
		ofSetColor(255,255);

		if( progress_angle > 0.1 )
		{
			//DRAW PROGRESS CIRCLE
			ofPath path = ofPath();
			if(!selected)
				path.setFillColor(ofColor(255,255,255,180));
			else
				path.setFillColor(ofColor(255,150,100,180));

			ofPoint pt(handAdaptor->getHandScaled().x, handAdaptor->getHandScaled().y);

			path.setCircleResolution(100);
			path.arc(pt, 50,50, 0, progress_angle, true);
			path.draw();
		}

	}

	ofImage * pimg;
	bool correct, centered;
};


class PointsBar
{
public:
	PointsBar()
{
		points = 0;
		bg.loadImage("../../../common-assets/barra_progresiva/bg.png");
		fg.loadImage("../../../common-assets/barra_progresiva/25.png");
}


	void setPoints( int pts )
	{
		points = pts;
		switch (points)
		{
		case 1:
			fg.loadImage("../../../common-assets/barra_progresiva/25.png");
			break;
		case 2:
			fg.loadImage("../../../common-assets/barra_progresiva/50.png");
			break;
		case 3:
			fg.loadImage("../../../common-assets/barra_progresiva/75.png");
			break;
		case 4:
			fg.loadImage("../../../common-assets/barra_progresiva/100.png");
			break;
		}
	}

	void display( ofVec2f pos )
	{
		bg.draw(pos);
		if(points>0)
			fg.draw(pos + ofVec2f(0*14, bg.getHeight() - fg.getHeight() - 0*22) /* so it looks centered, hardcoded */ );
	}

	int points;
	ofImage bg, fg;
};

class LifeBar
{
public:
	LifeBar()
{
		life = 0;
		green.loadImage("../../../common-assets/vida/cor-verd.png");
		red.loadImage("../../../common-assets/vida/cor-vermell.png");
}


	void setLife( int l, int m = 3 )
	{
		life = l;
		max_life = m;
	/*	stringstream ss;
		ss<<"../../../common-assets/vida/boto-cor" << life << ".png";
		fg.loadImage(ss.str()); */
	}

	void display( ofVec2f pos )
	{
		int count = 0;
		for( count=0; count<max_life - life; count++)
			red.draw( pos + ofVec2f(0,count*96) );

		for( count=max_life - life; count<max_life; count++)
			green.draw( pos + ofVec2f(0,count*96) );

		//bg.draw(pos);
		//fg.draw(pos + ofVec2f(100,-10) );
	}

	int life, max_life;
	ofImage green, red;
};


#define IDLE	0
#define HALF 	1
#define FULL	2

class MovingHand {

public:


	MovingHand( ofVec2f _p = ofVec2f(0,0), ofVec2f _s = ofVec2f(0,0), float _d = 10 )
{
		bg.loadImage("../../../common-assets/sayhello/fons-saluda.png");
		//		bg.setAnchorPercent(0.0,1.0);

		hand.loadImage("../../../common-assets/sayhello/boto-saluda.png");
		hand.setAnchorPercent(0.0,0.8);
		//	hand.resize(_s.x, _s.y);

		hand_bg.loadImage("../../../common-assets/sayhello/boto-saluda2.png");
		//	hand_bg.setAnchorPercent(0.5,0.5);
		//	hand_bg.resize(_s.x, _s.y);

		duration = _d;
		state = FULL;

		pos = _p;
		siz = _s;
		bar_length = siz.x;
		angle = 10;
		speed = 350;

		visible = true;
		progress = siz.y;

		sc=1.0;
}



	void start()
	{
		angle = 0;
		visible = true;
	}

	void hide()
	{
		visible = false;
	}

	bool isRunning()
	{
		return visible;
	}

	void scale(float _sc)
	{
		sc=_sc;
	}

	int getState()
	{
		return state;
	}

	void update( int hands_detected )
	{
		if( state == IDLE && hands_detected == 0 )
		{
			state = HALF;
			timeout = ofGetElapsedTimeMillis();
		}
		if( state == HALF && ( ofGetElapsedTimeMillis() - timeout) > duration*1000 )
		{
			state = FULL;
		}
		if( ((state == HALF) || (state == FULL)) && hands_detected > 0 )
		{
			state = IDLE;
		}

		if( state == IDLE ){
			guarda = ofGetElapsedTimeMillis();
			return;
		}

		float time = ofGetElapsedTimeMillis() /1000.0;
		draw_pos.x = pos.x    + 0.04*ofGetWidth()*sin( ofDegToRad(time)*speed ) * sc;
		draw_pos.y = pos.y    - 0.035*ofGetHeight()* cos( ofDegToRad(time)*speed )*cos( ofDegToRad(time)*speed ) * sc ;
		angle = 15*sin( ofDegToRad(time)*speed );

		if( state == HALF )
		{
			progress = ofMap(ofGetElapsedTimeMillis() - timeout, 0, duration*1000, 0, siz.y);
		}
	}

	void draw()
	{
		if( state == IDLE ) return;

		if( ofGetElapsedTimeMillis() - guarda < GUARDA_MOVING_HAND ) return;

		bg.draw(0, ofGetHeight() - bg.getHeight());
		//        if( state == HALF )
		//        {
		////            bg.draw(pos + ofVec2f(-120,-70) );
		//
		//            ofSetColor(100);
		//            ofRect( pos + ofVec2f(0,siz.y*1.1), bar_length, 15);
		//            ofSetColor(27, 86, 51);
		//            ofRect( pos + ofVec2f(0,siz.y*1.1), progress, 15);
		//            ofSetColor(255);
		//        }

		ofPushMatrix();
		ofTranslate(0,ofGetHeight()*0.9, 0);
		ofRotate(angle);
		hand_bg.setAnchorPercent(0.2, 0.8);
		hand_bg.draw(0, 0);
		/// NO VA... si cal ja ho mirarem altre cop
		//		  ofImage cropped_hand = hand;
		//		  		cropped_hand.crop(0,hand_bg.getHeight()-progress,hand_bg.getWidth(),progress);
		//		  		cropped_hand.draw(ofVec2f(0,/*hand_bg.getHeight()-progress*/0), hand_bg.getWidth(),progress );
		ofPopMatrix();



		//		ofPushMatrix();
		//		ofTranslate(draw_pos.x + sc * siz.x/2,draw_pos.y + sc * siz.y/2, 0);
		//		ofRotateZ(angle);
		//		ofTranslate(-draw_pos.x - sc * siz.x/2,-draw_pos.y - sc * siz.y/2, 0);
		//
		//		// draw the image
		//		hand_bg.draw(draw_pos,siz.x * sc,siz.y * sc);
		//		ofImage cropped_hand = hand;
		//		cropped_hand.crop(0,siz.y-progress,siz.x,progress);
		//		cropped_hand.draw(draw_pos + ofVec2f(0,siz.y-progress),siz.x * sc,progress * sc);
		//
		//		ofPopMatrix();
	}

	//----------------------------------------------------

	int state;
	int bar_length;
	long timeout, guarda;
	float duration;
	ofImage hand, hand_bg;
	ofVec2f pos, draw_pos, siz;
	float angle, speed;
	bool visible;
	int progress;
	ofImage bg;
	float sc;
};


#endif
