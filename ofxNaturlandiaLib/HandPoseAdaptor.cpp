/*
 * DynamicGestures.cpp
 *
 *  Created on: 26/02/2014
 *      Author: xaviersuaucuadros
 */

//#include "HandPoseAdaptor.h"

using namespace std;

static const float SWIPE_MIN_VEL = 3.0;//1.8;
static const float SWIPE_MIN_X_SPAN = 60.0;
static const float SWIPE_MAX_X_SPAN = 300.0;
static const float SWIPE_MAX_Y_SPAN = 30.0;

static const float PUSH_MIN_Z_SPAN = 60.0;
static const float PUSH_MAX_Z_SPAN = 300.0;
static const float PUSH_MIN_VEL    = 3.0;

void HandPoseAdaptor::update()
{
	updateHand(0.3);
	updateSwipe();
	updatePush();
}

DynamicGestures HandPoseAdaptor::getGestures()
{
	return gestures;
}


///Checks the hand ID to avoid "jumping" hands
void HandPoseAdaptor::updateHand( float filter )
{
	num_hands=handProvider->getNumHands();

	//Esperem count_first_frames, Evita alguna detecci� espor�dica observada...
	if( num_hands>0 && last_num_hands == 0)      count_first_frames = 0;
	if( num_hands>0 )                   		 count_first_frames++;


	if( num_hands>0 && count_first_frames > min_first_frames )
	{
		if(first_filter_frame)
		{
			first_filter_frame = false;
			hand = ofVec3f(0,0,0);
		}

		//change hand according to hand_ID, if needed
		int whichHand = 0;
		for( int i=0; i<num_hands; i++ )
		{
			if( handProvider->getHandId(i) == hand_ID )
			{
				whichHand = i;
				break;
			}
		}

		//update hand
		last_raw_hand = raw_hand;
		raw_hand = ofVec3f(handProvider->getHandX(whichHand),handProvider->getHandY(whichHand), handProvider->getHandZ(whichHand));
		uv_hand  = ofVec3f(handProvider->getHandU(whichHand),handProvider->getHandV(whichHand), 0);

		//if needed...
		last_hand = hand;
		filter_hand( raw_hand , hand, filter );
		filter_hand( uv_hand , uv_hand_f, filter );

		hand_ID = handProvider->getHandId(whichHand);

		//Update circular buffer
		hand_buffer.add( HandStamp( raw_hand, uv_hand, clock() ) );
		push_buffer.add( HandStamp( raw_hand, uv_hand, clock() ) );
	}
	else
	{
		hand = ofVec3f(-10000,-10000, -10000);
		first_filter_frame = true;
//		hand = ofVec3f(0,0,0);
		hand_buffer.clear();
		push_buffer.clear();
	}


	//Esperem count_first_frames, Evita alguna detecci� espor�dica observada...
	last_num_hands = num_hands;
	if( count_first_frames<=min_first_frames )            num_hands = 0;

}



// Sets and filters the hand
void HandPoseAdaptor::filter_hand(ofVec3f & newhand, ofVec3f & hand, float filter )
{
	//last_hand = hand;
	hand += ( newhand - hand )*filter;

//	// First predict, to update the internal statePre variable
//	cv::Mat prediction = KF.predict();
//	cv::Point predictPt(prediction.at<float>(0),prediction.at<float>(1));
//
//	// Get mouse point
//	cv::Mat_<float> measurement(2,1); measurement.setTo(cv::Scalar(0));
//	measurement(0) = newhand.x;
//	measurement(1) = newhand.y;
//
//	cv::Point measPt(measurement(0),measurement(1));
//
//	// The "correct" phase that is going to use the predicted value and our measurement
//	cv::Mat estimated = KF.correct(measurement);
//	cv::Point statePt(estimated.at<float>(0),estimated.at<float>(1));
//
//	//Output
//	hand.x = statePt.x;
//	hand.y = statePt.y;
//	hand.z = newhand.z;
}


float HandPoseAdaptor::sign_of( float val )
{
	if( val >= 0 ) return 1.0;
	else            return -1.0;
}

///
/// detects a swipe in the left_direction (or right if false)
///
/// \return bool = true if push movement is detected
///
void HandPoseAdaptor::updateSwipe( bool left_direction )
{
	//clear the buffer if no hands detected
	if( handProvider->getNumHands() == 0 )
	{
		if(hand_buffer.get_N() != 0)
			hand_buffer.clear();
		return;
	}

	float sign = 1.0;
	if (left_direction) sign = -1.0;


	HandStamp 	max_span = hand_buffer.get_max_span();
	bool 		coherent = hand_buffer.coherent_x_movement(sign);

//	std::cout<<"--- swipe || span: "<<max_span.hand<<" , maxspeed: "<<fabs( hand_buffer.max_x_speed(sign) ) <<std::endl;

	if( coherent &&
		fabs(max_span.hand.y < SWIPE_MAX_Y_SPAN) &&						//Restrict y span
		fabs(max_span.hand.x) > SWIPE_MIN_X_SPAN &&						//Restrict to long swipes
		fabs(max_span.hand.x) < SWIPE_MAX_X_SPAN &&						//Restrict to short swipes
		fabs( hand_buffer.max_x_speed(sign) ) > SWIPE_MIN_VEL  )		//Restrict to fast swipes
	{
		std::cout<<"--- swipe || span: "<<max_span.hand<<" , maxspeed: "<<fabs( hand_buffer.max_x_speed(sign) ) <<std::endl;
		//fill the output struct
		gestures.swipe = true;
		gestures.swipe_pos = hand_buffer.avg_hand();
		//clear buffer
		hand_buffer.clear();
	}
	else
	{
		gestures.swipe = false;
	}
}

void HandPoseAdaptor::updatePush()
{
	//clear the buffer if no hands detected
	if( handProvider->getNumHands() == 0 )
	{
		if(push_buffer.get_N() != 0)
			push_buffer.clear();
		return;
	}

	HandStamp 	max_span = push_buffer.get_max_span();
	bool 		coherent = push_buffer.coherent_push_movement();

//	std::cout<<"--- swipe || span: "<<max_span.hand<<" , maxspeed: "<<fabs( hand_buffer.max_x_speed(sign) ) <<std::endl;

	if( coherent &&
		fabs(max_span.hand.z) > PUSH_MIN_Z_SPAN &&						//Restrict to long swipes
		fabs(max_span.hand.z) < PUSH_MAX_Z_SPAN &&						//Restrict to short swipes
		fabs( push_buffer.max_z_speed(-1) ) > PUSH_MIN_VEL  )		//Restrict to fast swipes
	{
		std::cout<<"--- push OOO span: "<<max_span.hand<<" , maxspeed: "<<fabs( push_buffer.max_z_speed(-1) ) <<std::endl;
		//fill the output struct
		gestures.push = true;
		gestures.push_pos = push_buffer.avg_hand();
		//clear buffer
		push_buffer.clear();
	}
	else
	{
		gestures.push = false;
	}
}


// Scale to screen size. Takes hand depth into account, so that for farther distances, a user placed +/- centered can reach the whole screen without moving his feet.'
    void HandPoseAdaptor::scaleToScreen( ofVec2f & h, double d )
    {
        ofVec2f hc = h - ofVec2f(160.0, 120.0);
        double factor  = (70.0 - 320.0)/(2000.0 - 0.0 )*d + 320;
        double factor2 = factor * 240.0 / 320.0 ;

        hc.x = hc.x * ofGetWidth()/factor;
        hc.y = hc.y * ofGetHeight()/factor2;

        h = hc + ofVec2f( ofGetWidth()*0.5, ofGetHeight()*0.5 );
    }
