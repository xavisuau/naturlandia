//
//  question.h
//  jocNaturlandia
//
//  Created by Xavier Suau Cuadros on 19/10/13.
//
//

#ifndef jocNaturlandia_question_h
#define jocNaturlandia_question_h

#include "GUIelements.h"
#include "ofxTween.h"


class Question{
    
public:
    
    Question(){}
    ~Question(){}
    
    vector<GUIselectablePImage* >  answerIcons;
    int answer, not_answer;
    long timeout, MAX_TO;
    int status; //0=not answered, 1=answered, 2=can jump to next question
    float winSize;
    std::string text;

    
    Question(  std::string _qStr, GUIselectablePImage * _y, GUIselectablePImage * _n, int _a)
    {
        init( _qStr, _y, _n, _a);
    }
    
//    Question(  std::string _qStr, std::string _yesStr, std::string _noStr, int _a, HandPoseAdaptor * _ha)
//    {
//        GUIselectableImage _y = GUIselectableImage(ofGetWidth()*0.25,500,"Si",_yesStr, _ha, true);
//        GUIselectableImage _n = GUIselectableImage(ofGetWidth()*0.75,500,"No",_noStr, _ha, true);
//        init( _qStr,  _y, _n , _a);
//    }
    
    void init(std::string _qStr, GUIselectablePImage * _y, GUIselectablePImage * _n, int _a)
    {
        text = _qStr;
        cropString( text, 40 );
        answerIcons.push_back( _y );
        answerIcons.push_back( _n );
        answer = _a;
        not_answer = (answer+1)%2;
        reset();
    }
    
    int cropString( std::string & str, int maxwidth )
    {
        int spacepos = 0, line = 1;
        while( spacepos != std::string::npos )
        {
            spacepos = str.find(" ", spacepos+1);
            if( spacepos >= maxwidth*line && spacepos != std::string::npos)
            {
                str.replace( spacepos, 1, "\n" );
                line++;
            }
        }
    }
    

    
    int updateStatus(int push)
    {
        
        for( int i=0; i<answerIcons.size(); i++ )
            answerIcons[i]->updateWithPush(push);
        
        
        if( answerIcons[ answer ]->selected && status == 0 )
        {
            status = 1;
            timeout = ofGetElapsedTimeMillis();
            std::cout<<" status 1"<<std::endl;
        }
        
        
        if( answerIcons[ not_answer ]->selected && status == 0 )
        {
            status = -1;
            timeout = ofGetElapsedTimeMillis();
            std::cout<<" status -1"<<std::endl;
        }
        
        if( status == 1 || status == -1 )
        {
            if( ofGetElapsedTimeMillis() - timeout < MAX_TO )
            {
                winSize += (500 - winSize)*0.1;
            }
            else
            {
                status = 2*status; //2 or -2
                std::cout<<" status "<<status<<std::endl;
                
            }
        }
        
        
        for( int i=0; i<answerIcons.size(); i++ )
            answerIcons[i]->selected = false;
        
        
        return status;
    }
    
    void reset()
    {
        status = 0;
        winSize = 0;
        MAX_TO = 3000;
    }
    
    void draw(ofTrueTypeFont * font)
    {
        ofSetColor(0,210);
        //Center Text
        float textW = font->stringWidth( text);
        float posX = ofGetWidth()*0.13;
        float textSpace = ofGetWidth() - posX*2;
        font->drawString(text, posX + (textSpace - textW)/2 ,  ofGetHeight()*0.3 );
        ofSetColor(255);
        
        if( answerIcons[0]->handOver() ) ofSetColor(255,255);
        else                             ofSetColor(255,150);
        answerIcons[0]->display();
        if( answerIcons[1]->handOver() ) ofSetColor(255,255);
        else                             ofSetColor(255,150);
        answerIcons[1]->display();
        ofSetColor(255,255);
    }
    
    
    
};



class QuestionList
{
public:
    QuestionList( )
    {
        correct.loadImage("common/correct.png");
        wrong.loadImage("common/wrong.png");
        bariol.loadFont("Bariol-Bold.otf", 30, true, true);
        bariol.setLineHeight(40.0f);
        bariol.setLetterSpacing(0.95);
        bariol.setEncoding(OF_ENCODING_UTF8);
    }
    ~QuestionList( ){}

    void addQuestion( Question  q )
    {
        list.push_back(q);
    }
    
    void drawQuestion( int idx )
    {

        list[idx].draw( &bariol );
        
        int winSize = list[idx].winSize;
        if( list[idx].status == 1)
        {
            correct.draw( ofGetWidth()/2 - winSize/2, ofGetHeight()/2 - winSize/2, winSize, winSize);
        }
        if( list[idx].status == -1)
        {
            wrong.draw( ofGetWidth()/2 - winSize/2, ofGetHeight()/2 - winSize/2, winSize, winSize);
        }
    }
    
    int size()
    {
        return list.size();
    }
    
    ofImage  correct, wrong;
    ofTrueTypeFont	bariol;
    std::vector<Question> list;
    
};

/*
class MapQuestion{
    
public:
    MapQuestion(){}
    
    MapQuestion( ofVec2f _winpos, GUIselectableDraggablePImage * _icon, std::string _n)
    {
    	name = _n;
        icon = _icon;
        winpos = _winpos;
        initPos  = icon->pos;
        initSize = icon->siz;
        won = false;
        failed = false;
    	untouched = true;

        //HOT Spot size
        W = 200;
        H = 200;

    	mark.loadImage("game/boto-gps.png");
    	mark.setAnchorPercent(0.5,0.5);
    	mark_wrong.loadImage("game/boto-gps-incorrecte.png");
    	mark_wrong.setAnchorPercent(0.5,0.5);
    	mark_correct.loadImage("game/boto-gps-correcte.png");
    	mark_correct.setAnchorPercent(0.5,0.5);

    	std::cout<<"before"<<std::endl;
    	markY.setParameters(1,easingbounce,ofxTween::easeOut, 1200, 0, 2000.0, std::rand()%1000 );
    	std::cout<<"after"<<std::endl;

    }
    
    bool isIconOnZone()
    {
        ofVec2f iconC = icon->pos +icon->siz/2;
        return ( iconC.x > winpos.x - W/2 &&
                 iconC.x < winpos.x + W/2 &&
                 iconC.y > winpos.y - H/2 &&
                 iconC.y < winpos.y + H/2 );
    }
    
    void update( bool push = false)
    {

        if( !won )
            icon->updateWithPush(push);
         //  icon->updateSelectProgress(  );

        //icon->updateWithPush( push );
        
        if( icon->selected && untouched){
            icon->setPos(       initPos.x+icon->siz.x*0.5,  initPos.y+icon->siz.y*0.5  );
            icon->setSize(      initSize.x * 0.2, initSize.y * 0.2 );
        	untouched = false;
        }

        if( !won && isIconOnZone() && icon->justReleased )
        {
            won = true;
            failed = false;
            icon->progress_angle = -1;
            icon->setPos(winpos.x - icon->siz.x/2, winpos.y -  icon->siz.y/2);
        }
        if( icon->justReleased && !isIconOnZone() )
        {
        	failed = true;
        }
    }
    
    void draw( bool showicon = true )
    {

    	if(!won && !failed)
    	{
 		mark.draw(winpos + ofVec2f(0, -markY.update()));
    	}
    	if( won || failed || showicon )
    	{
    		icon->display();

//    		if( won )
//    			icon->display(ofVec2f(icon->siz.x*0.5,icon->siz.y*0.5), ofVec2f(0.2,0.2));
//    		else
//    		{
//    			if( untouched )
//    				icon->display();
//    			else
//    				icon->display(ofVec2f(icon->siz.x*0.5,icon->siz.y*0.5), ofVec2f(0.2,0.2));
//
//    		}
    	}

    	if( won )
    	{
    		ofSetColor(255,120);
    		mark_correct.draw(winpos);
    		ofSetColor(255,255);
    	}
    	if( failed ) mark_wrong.draw(winpos);
        
        ofSetColor(255,255);
            
    }
    
    void reset()
    {
        icon->setPos(       initPos.x,  initPos.y  );
        icon->setSize(      initSize.x, initSize.y );
        icon->selected =    false;
        icon->correct  =    false;
        won = false;
        failed = false;
        icon -> reset();
    	markY.setParameters(1,easingbounce,ofxTween::easeOut, 1200, 0, 2000.0, std::rand()%1000);
    	untouched = true;
    }

    
    ofImage mark, mark_correct, mark_wrong;
    float W,H;
    GUIselectableDraggablePImage * icon;
    ofVec2f initPos, initSize, winpos;
    bool won, failed, untouched;
    std::string name;

	ofxEasingBounce      easingbounce;
	ofxTween markY;
};
*/


struct SoundConf
{
    ofImage         img;
    std::vector<ofImage> images;
    ofSoundPlayer   sound;
    std::string name;
};


class SoundQuestion : public GUIselectablePImage{
    
public:
   
    ofImage * correct, * wrong, * frame;
    ofSoundPlayer * sound, * correctSound;
    bool answer;
    long timeout, MAX_TO;
    int status; //0=not answered, 1=correct answered, 2=can jump to next question+success, -1=wrong answered, -2=can jump to next question+failed
    float winSize;
    
    

    SoundQuestion(){}
    ~SoundQuestion(){}
    SoundQuestion(ofVec2f  _pos, ofVec2f  _siz, ofSoundPlayer * _sound, ofImage * _pimg, HandPoseAdaptor * _ha, bool centered = false) : GUIselectablePImage(_pos,_siz," ",_pimg,_ha, centered)
    {
        sound = _sound;
    }
    
    void setAnswer( bool  _answer )
    {
        answer = _answer;
    }
    
    void setFrame( ofImage * _f)
    {
    	frame = _f;
    }

    void setCorrectWrong( ofImage * _correct, ofImage * _wrong, ofSoundPlayer * _correctSound)
    {
        correct = _correct;
        wrong = _wrong;
        correctSound = _correctSound;
    }
    
    
    void setImage( ofImage * _pimg )
    {
    	pimg = _pimg;
    }

    int updateStatus( bool push )
    {
        
        updateWithPush(push);
//        if( selected && correct  && status == 0 )
//        {
//            status = 1;
//            timeout = ofGetElapsedTimeMillis();
//            correctSound->play();
//            std::cout<<" status 1"<<std::endl;
//        }
//        
//        
//        if( selected && !correct && status == 0 )
//        {
//            status = -1;
//            timeout = ofGetElapsedTimeMillis();
//            std::cout<<" status -1"<<std::endl;
//        }
//        
//        if( status == 1 || status == -1 )
//        {
//            if( ofGetElapsedTimeMillis() - timeout < MAX_TO )
//            {
//                winSize += (500 - winSize)*0.1;
//            }
//            else
//            {
//                status = 2*status; //2 or -2
//                std::cout<<" status "<<status<<std::endl;
//                
//            }
//        }
//        
//        if( fabs(status) == 2 ){
//            winSize = 0;
//            status = 0;
//        }
//

        
        //SOUND HANDLING
        if( answer  && sound->getIsPlaying() == false  )
        {
            sound->play();
        }
        
//        if( !answer && sound->getIsPlaying() == true )
//        {
//            sound->stop();
//        }
        
        return status;
    }
    
    void reset()
    {
        status = 0;
        winSize = 0;
        MAX_TO = 3000;
        sound->stop();
        correct = 0;
       // selected = 0;
    }
    
    
    void pauseSound()
    {
        sound->stop();
    }
    
    void draw()
    {
        if( handOver() )  ofSetColor(150);
        
        frame->draw(pos, siz.x, siz.y);
        display();

        /////// DEBUG, shows correct answer
        /*
        ofSetColor(255, 100);
        
        if(answer)
            ofCircle( pos + siz/2, 30);
            */

        ofSetColor(255, 255);

    }
    
    void drawCorrect()
    {        
        if( status == 1)
        {
            correct->draw( ofGetWidth()/2 - winSize/2, ofGetHeight()/2 - winSize/2, winSize, winSize);
        }
        if( status == -1)
        {
            wrong->draw( ofGetWidth()/2 - winSize/2, ofGetHeight()/2 - winSize/2, winSize, winSize);
        }
        
    }
    
    
    
};

class SimpleQuestion
{
public:
    
    ofImage img;
    int answer;
    
    SimpleQuestion( std::string _qStr, int _a)
    {
        img.loadImage( _qStr );
        answer = _a;
    }
    
    ~SimpleQuestion(){}

    bool isCorrect(int _a)
    {
        return answer == _a;
    }
    
    void draw()
    {
        img.draw( ofGetWidth()*0.55, ofGetHeight()*0.2, ofGetWidth()*0.42, ofGetWidth()*0.42*img.getHeight()/img.getWidth() );
    }
};

class Petjada{
    
public:
    
    ofImage petjada;
    ofVec2f pos;
    float pressure, MAX_PRESSURE;
    long count, TIMEOUT;
    bool selected;
    int status;
    Petjada(){}
    ~Petjada(){}
    Petjada(   std::string _petjadaStr, int _x, int _y )
    {
        petjada.loadImage( _petjadaStr );
        pos = ofVec2f(_x, _y);
        selected = false;
        reset();
        MAX_PRESSURE = 30;
        TIMEOUT = 700;
    }

    
    void reset()
    {
        status = 0;
        count = -1;
        selected = false;
    }
    
    void update( float _pressure)
    {
        if( selected ) selected == false;
        
        pressure = _pressure;
        
        if( _pressure > MAX_PRESSURE && count < 0)
        {
            count = ofGetElapsedTimeMillis();
            status = 1;
        }
        
        if (_pressure > MAX_PRESSURE )
        {
            
            if( ofGetElapsedTimeMillis() - count > TIMEOUT )
            {
                status = 2;
                count = -1;
                selected = true;
            }
        }
        else
        {
            count = -1;
            status = 0;
        }
    }
    
    void draw()
    {
        ofSetColor(255,255);
        petjada.draw(pos.x, pos.y);
        ofSetColor(255,255);
        

    }

    void drawProgress()
    {
    	   if( status == 1 && ofGetElapsedTimeMillis() - count < TIMEOUT) {
    	            ofPath path = ofPath();
    	            path.setFillColor(ofColor(100,255,200,100));

    	            float angle = ofMap( ofGetElapsedTimeMillis() - count, 0, TIMEOUT, 0, 360 ) + 0.1;
    	            path.setCircleResolution(100);
    	            path.arc(ofVec2f(850, 540), 150,150, 0, angle, true);
    	            path.draw();
    	        }
    }
};

#endif
