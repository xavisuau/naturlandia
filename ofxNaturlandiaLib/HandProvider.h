

#ifndef handProvider_h
#define handProvider_h

#include "NiTE.h"

///
///
/// \brief Hand Provider Implements Hand Tracking and Basic Hand Gesture Recognition
///
/// It initializes tracking with Waving Gesture.
///

class HandProvider
{
public:

	HandProvider()
	{

	}

	~HandProvider()
	{
		stop();
	}

	/// Init client
	inline void init();

	/// Main method, to be called within a loop
	///
	/// Reads frame detects gesture and tracks whenever it has been initialized
	inline void process();

	/// Stop client
	inline void stop();

	/// Get number of tracked hands
	inline int getNumHands() const;

	/// Get identifier of the i-th hand
	inline short int getHandId( int i);

	/// Get X position (world coordinates) of the i-th hand
	inline double getHandX( int i);
    
	/// Get Y position (world coordinates) of the i-th hand
	inline double getHandY( int i);
    
	/// Get Z position (world coordinates) of the i-th hand
	inline double getHandZ( int i);

	/// Get X position (in the image plane) of the i-th hand
	inline double getHandU( int i);

	/// Get Y position (in the image plane) of the i-th hand
	inline double getHandV( int i);


private:

	nite::HandTracker handTracker;
	nite::Status niteRc;
	nite::HandTrackerFrameRef handTrackerFrame;
};

#include "HandProvider.cpp"

#endif
