//#include "PoseProvider.h"




void PoseProvider::_updateUserState(const nite::UserData& user, unsigned long long ts)
{
	if (user.isNew())
		USER_MESSAGE("New")
	else if (user.isVisible() && !g_visibleUsers[user.getId()])
		USER_MESSAGE("Visible")
	else if (!user.isVisible() && g_visibleUsers[user.getId()])
		USER_MESSAGE("Out of Scene")
	else if (user.isLost())
		USER_MESSAGE("Lost")

	g_visibleUsers[user.getId()] = user.isVisible();


	if(g_skeletonStates[user.getId()] != user.getSkeleton().getState())
	{
		switch(g_skeletonStates[user.getId()] = user.getSkeleton().getState())
		{
		case nite::SKELETON_NONE:
			USER_MESSAGE("Stopped tracking.")
			break;
		case nite::SKELETON_CALIBRATING:
			USER_MESSAGE("Calibrating...")
			break;
		case nite::SKELETON_TRACKED:
			USER_MESSAGE("Tracking!")
			break;
		case nite::SKELETON_CALIBRATION_ERROR_NOT_IN_POSE:
		case nite::SKELETON_CALIBRATION_ERROR_HANDS:
		case nite::SKELETON_CALIBRATION_ERROR_LEGS:
		case nite::SKELETON_CALIBRATION_ERROR_HEAD:
		case nite::SKELETON_CALIBRATION_ERROR_TORSO:
			USER_MESSAGE("Calibration Failed... :-|")
			break;
		}
	}
}

void PoseProvider::init()
{
	niteRc = nite::NiTE::initialize();
		if (niteRc != nite::STATUS_OK)
		{
			printf("User Tracker initialization failed\n");
			exit(1);
		}

	niteRc = userTracker.create();
	if (niteRc != nite::STATUS_OK)
	{
		printf("Couldn't create user tracker\n");
		exit(3);
	}
	printf("\nStart moving around to get detected...\n(PSI pose may be required for skeleton calibration, depending on the configuration)\n");
}


void PoseProvider::process()
{
	niteRc = userTracker.readFrame(&userTrackerFrame);
	if (niteRc != nite::STATUS_OK)
	{
		printf("Get next frame failed\n");
		return;
	}

	const nite::Array<nite::UserData>& users = userTrackerFrame.getUsers();
	for (int i = 0; i < users.getSize(); ++i)
	{
		const nite::UserData& user = users[i];
		_updateUserState(user,userTrackerFrame.getTimestamp());
		if (user.isNew())
		{
			userTracker.startSkeletonTracking(user.getId());
		}
//		else if (user.getSkeleton().getState() == nite::SKELETON_TRACKED)
//		{
//			const nite::SkeletonJoint& head = user.getSkeleton().getJoint(nite::JOINT_HEAD);
//			if (head.getPositionConfidence() > .5)
//			printf("%d. (%5.2f, %5.2f, %5.2f)\n", user.getId(), head.getPosition().x, head.getPosition().y, head.getPosition().z);
//		}
	}

}

void PoseProvider::stop()
{
	nite::NiTE::shutdown();
	openni::OpenNI::shutdown();
}



int PoseProvider::getNumUsers()
{
	const nite::Array<nite::UserData>& users = userTrackerFrame.getUsers();
	return users.getSize();
}

short int PoseProvider::getUserId(int i)
{
	const nite::Array<nite::UserData>& users = userTrackerFrame.getUsers();
	const nite::UserData& user = users[i];
	return user.getId();
}

double PoseProvider::getHeadX (int i)
{
	const nite::Array<nite::UserData>& users = userTrackerFrame.getUsers();
	const nite::UserData& user = users[i];
	if (user.getSkeleton().getState() == nite::SKELETON_TRACKED)
	{
		const nite::SkeletonJoint& head = user.getSkeleton().getJoint(nite::JOINT_HEAD);
		if (head.getPositionConfidence() > .5)
			return head.getPosition().x;
		else
			return -10000;
	}
}

double PoseProvider::getHeadY (int i)
{
	const nite::Array<nite::UserData>& users = userTrackerFrame.getUsers();
	const nite::UserData& user = users[i];
	if (user.getSkeleton().getState() == nite::SKELETON_TRACKED)
	{
		const nite::SkeletonJoint& head = user.getSkeleton().getJoint(nite::JOINT_HEAD);
		if (head.getPositionConfidence() > .5)
			return head.getPosition().y;
		else
			return -10000;
	}
}

double PoseProvider::getHeadZ (int i)
{
	const nite::Array<nite::UserData>& users = userTrackerFrame.getUsers();
	const nite::UserData& user = users[i];
	if (user.getSkeleton().getState() == nite::SKELETON_TRACKED)
	{
		const nite::SkeletonJoint& head = user.getSkeleton().getJoint(nite::JOINT_HEAD);
		if (head.getPositionConfidence() > .5)
			return head.getPosition().z;
		else
			return -10000;
	}
}

double PoseProvider::getHeadU (int i)
{
	const nite::Array<nite::UserData>& users = userTrackerFrame.getUsers();
	const nite::UserData& user = users[i];
	if (user.getSkeleton().getState() == nite::SKELETON_TRACKED)
	{
		const nite::SkeletonJoint& head = user.getSkeleton().getJoint(nite::JOINT_HEAD);
		if (head.getPositionConfidence() > .5)
		{
			float u, v;
			openni::DepthPixel depth;

			niteRc = userTracker.convertJointCoordinatesToDepth 	(  head.getPosition().x,
					 head.getPosition().y,
					 head.getPosition().z,
					&u,
					&v
				);
			return u;
		}
		else
			return -10000;
	}
}

double PoseProvider::getHeadV (int i)
{
	const nite::Array<nite::UserData>& users = userTrackerFrame.getUsers();
	const nite::UserData& user = users[i];
	if (user.getSkeleton().getState() == nite::SKELETON_TRACKED)
	{
		const nite::SkeletonJoint& head = user.getSkeleton().getJoint(nite::JOINT_HEAD);
		if (head.getPositionConfidence() > .5)
		{
			float u, v;
			openni::DepthPixel depth;

			niteRc = userTracker.convertJointCoordinatesToDepth 	(  head.getPosition().x,
					 head.getPosition().y,
					 head.getPosition().z,
					&u,
					&v
				);
			return v;
		}
		else
			return -10000;
	}
}



