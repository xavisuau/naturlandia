#ifndef SCREENLANGUAGE_H_
#define SCREENLANGUAGE_H_

#pragma once

#include "HandTrackerThread.h"

#include "ofMain.h"
#include "ofxXmlSettings.h"

//Include screens to be used
#include "ScreenWelcome.h"
#include "ScreenLanguage.h"
#include "ScreenGame.h"
#include "ScreenExplanation.h"


class ScreenManager : public ofBaseApp
{
public:

	ScreenManager() : ofBaseApp()
	{}

	~ScreenManager()
	{
			std::cout << "Destroying " << screens.size() << "  screens" << std::endl;
			//Do we free a map of pointers this way?
			for( std::map< int, NatScreen *>::iterator it = screens.begin(); it != screens.end(); it++)
			{
				delete it->second;
			}
			std::cout << "Done with screens\n" << std::flush;
		//	screens.clear();

	}

	void setup()
	{
		//Tracker
		tracker.init();

		handAdaptor = HandPoseAdaptor(&tracker, 3);

		//Capture and Tracking thread
		cap.init( &tracker, &handAdaptor );
		cap.start(); //non-blocking, verbose

		/*
		objscreens.push_back(ScreenWelcome());
		objscreens.push_back(ScreenLanguage());
		objscreens.push_back(ScreenExplanation());
		objscreens.push_back(ScreenGame());
		screens[SCREEN_WELCOME] 		= &objscreens[0] ;
		screens[SCREEN_LANGUAGE] 		= &objscreens[1] ;
		screens[SCREEN_EXPLANATION] 	= &objscreens[2] ;
		screens[SCREEN_GAME] 			= &objscreens[3] ;
		*/
		
				//Add screens here
		screens[SCREEN_WELCOME] 		= new ScreenWelcome() ;
		screens[SCREEN_LANGUAGE] 		= new ScreenLanguage() ;
		screens[SCREEN_EXPLANATION] 	= new ScreenExplanation() ;
		screens[SCREEN_GAME] 			= new ScreenGame() ;
		active_screen = SCREEN_WELCOME;

		cursor.loadImage("../../../common-assets/cursor.png");
		cursor.setAnchorPercent(0.5, 0.5);
		moving_hand = MovingHand();//"common/hand.png", ofVec2f( ofGetWidth()/2, ofGetHeight()*0.6), ofVec2f(120,120), 10);


		//Setup added screens
		for( std::map< int, NatScreen *>::iterator it = screens.begin(); it != screens.end(); it++)
		{
			it->second->handAdaptor = &handAdaptor;  // <--- Assign hand adaptor, do this before setup() !
			it->second->setDynamics( &dyngest );
			it->second->setup();
		}

	}

	void update()
	{

		screens[active_screen]->update();

		//Check if the active screen is telling to jump to another screen
		ScreenInfo next_info = screens[active_screen]->nextScreen();

		if( next_info.code >= 0 )
		{
			screens[active_screen]->do_specific_things();
			active_screen = next_info.code;
			screens[active_screen]->setLanguage(next_info.language);
			screens[active_screen]->reset();
			//screens[active_screen]->setup();
		}

		moving_hand.update(handAdaptor.numHands() );
		dyngest = handAdaptor.getGestures();


/*
 * go back to welcome when hand timeout is over
 */
		if( moving_hand.getState() == FULL && active_screen != SCREEN_WELCOME && screens[active_screen]->show_hand )
		{
			screens[active_screen]->do_specific_things();
			std::string lang = screens[active_screen]->language;
			active_screen = SCREEN_WELCOME;
			screens[active_screen]->setLanguage(lang);
		}
	}

	void draw()
	{
		screens[active_screen]->draw();
		//		std::cout<<"hand "<<handAdaptor.uv_hand<<std::endl;

		if( handAdaptor.numHands() > 0 && screens[active_screen]->show_hand)
			cursor.draw( handAdaptor.getHandScaled().x,handAdaptor.getHandScaled().y);//, 20, 20);

		if( screens[active_screen]->show_hand )
			moving_hand.draw();
	}


	void exit()
	{
		/*
			std::cout << "Thread stop" << std::endl;
	//	cap.stopThread();
	//	cap.waitForThread(true); //This function should block until the thread is stopped (true)
		sleep(3);
		std::cout<<"stopped"<<std::endl;

		std::cout << "Tracker stop" << std::endl;
		tracker.stop();
		sleep(3);
		std::cout<<"stopped"<<std::endl;

*/
		/*	std::cout << "Memory free" << std::endl;
		//Do we free a map of pointers this way?
		for( std::map< int, NatScreen *>::iterator it = screens.begin(); it != screens.end(); it++)
		{
			delete it->second;
		}
		screens.clear();*/

	}

	void keyPressed(int key){

	switch(key) {


	case 27:

		std::cout << "Thread stop... " << std::flush;
		cap.stopThread();
		cap.waitForThread(true); //This function should block until the thread is stopped (true)
		std::cout<<"stopped"<<std::endl;

		std::cout << "Tracker stop... " << std::flush;
		tracker.stop();
		std::cout<<"stopped"<<std::endl;

		break;

	}


}


	//Tracking
	HandTrackerThread   cap;
	HandProvider        tracker;
	HandPoseAdaptor     handAdaptor;

	//Screens
	std::map< int, NatScreen *> screens;
	int active_screen;

	/*std::vector<NatScreen> objscreens;*/

	ofImage cursor;
	MovingHand moving_hand;
	DynamicGestures dyngest;
};

#endif
