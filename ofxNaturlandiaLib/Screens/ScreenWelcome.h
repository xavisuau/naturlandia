/*
 * ScreenWelcome.h
 *
 *  Created on: 24/02/2014
 *      Author: xaviersuaucuadros
 */

#ifndef SCREENWELCOME_H_
#define SCREENWELCOME_H_

#include "Screens.h"
#include "GUIelements.h"

class ScreenWelcome : public NatScreen{

public:

	ScreenWelcome() : NatScreen(){}
	~ScreenWelcome()
	{
		//std::cout << "Destroying Welcome " << std::endl; 
	}

	void setup()
	{

		/*
		 * init common vars
		 */
		NatScreen::initCommonVars();

			leaves_bottom.loadImage("../../../common-assets/plantes_baix.png");
			leaves_top.loadImage("../../../common-assets/plantes_dalt_pinya.png");

		quadre.loadImage("../../../common-assets/welcome/quadre.png");
//
		//Buttons
/*		play_button[LANG_CAT] =  GUIselectableImage(0, 0, "PLAYBUTTON", "../../../common-assets/welcome/boto_jugar_CAT.png", handAdaptor, true) ;
		play_button[LANG_CAT].setHoverImage("../../../common-assets/welcome/boto_jugar_hover_CAT.png");
		play_button[LANG_ESP] =  GUIselectableImage(0, 0, "PLAYBUTTON", "../../../common-assets/welcome/boto_jugar_ESP.png", handAdaptor, true) ;
		play_button[LANG_ESP].setHoverImage("../../../common-assets/welcome/boto_jugar_hover_ESP.png");
		play_button[LANG_ENG] =  GUIselectableImage(0, 0, "PLAYBUTTON", "../../../common-assets/welcome/boto_jugar_ENG.png", handAdaptor, true) ;
		play_button[LANG_ENG].setHoverImg("../../../common-assets/welcome/boto_jugar_hover_ENG.png");
		play_button[LANG_FRA] =  GUIselectableImage(0, 0, "PLAYBUTTON", "../../../common-assets/welcome/boto_jugar_FRA.png", handAdaptor, true) ;
		play_button[LANG_FRA].setHoverImage("../../../common-assets/welcome/boto_jugar_hover_FRA.png");
		play_button[LANG_RUS] =  GUIselectableImage(0, 0, "PLAYBUTTON", "../../../common-assets/welcome/boto_jugar_RUS.png", handAdaptor, true) ;
		play_button[LANG_RUS].setHoverImage("../../../common-assets/welcome/boto_jugar_hover_RUS.png");
*/
		for( int i=0; i<lang_vec.size(); i++)
		{
			play_button[ lang_vec[i] ] =  GUIselectableImage(0, 0, "PLAYBUTTON", "../../../common-assets/welcome/boto_jugar_hover_" + lang_vec[i] + ".png", handAdaptor, true) ;
			play_button[ lang_vec[i] ].setHoverImage("../../../common-assets/welcome/boto_jugar_"+lang_vec[i]+".png");
			play_button[ lang_vec[i] ].setPos(ofGetWidth() * 550.0/1920.0, ofGetHeight() * 540.0/1080.0);
			play_button[ lang_vec[i] ].setSize( 388,233 );

			lang_button[ lang_vec[i] ] =  GUIselectableImage(0, 0, "LANGBUTTON", "../../../common-assets/welcome/" + lang_vec[i] + ".png", handAdaptor, true) ;
			lang_button[ lang_vec[i] ].setHoverImage("../../../common-assets/lang/"+lang_vec[i]+".png");
			lang_button[ lang_vec[i] ].setPos(ofGetWidth() * 997.0/1920.0, ofGetHeight() * 540.0/1080.0);
			lang_button[ lang_vec[i] ].setSize( 388,233 );
		}

/*		std::map<std::string, GUIselectableImage>::iterator it = play_button.begin();
		for( ; i!=play_button.end(); it++)
		{
			it->second.setPos(ofGetWidth() * 550.0/1920.0, ofGetHeight() * 540.0/1080.0);
			it->second.setSize( 388,233 );
		}




		lang_button =  GUIselectableImage(0, 0, "LANGBUTTON", "../../../common-assets/welcome/CAT.png", handAdaptor, true) ;
		lang_button.setPos(ofGetWidth() * 997.0/1920.0, ofGetHeight() * 540.0/1080.0);
		lang_button.setSize( 388,233 );
*/
		//
		load_language_data();
		reset();

	}
	void load_language_data()
	{
		//now load that same file and get the values out
		ofxXmlSettings settings;
		settings.loadFile("strings.xml");
		titleStr = settings.getValue(language + ":game_title", "HOLA!");
	}
	void update()
	{
		next_screen = ScreenInfo();

		//Updates the question and explanation buttons
		updateExplanation();
		if( state == STATE_EXPLANATION ) return;


		if( CurrTime() < 1500 )
		{
			lang_button[language].reset();
			play_button[language].reset();
			return;
		}

		// lang_button.updateSelectProgress();
		
		lang_button[language].updateWithPush(dyngest->push);
		play_button[language].updateWithPush(dyngest->push);
		
		if( lang_button[language].selected )
		{
			lang_button[language].reset();
			next_screen = ScreenInfo(SCREEN_LANGUAGE, language);
		}

		// play_button.updateSelectProgress();
		if( play_button[language].selected )
		{
			play_button[language].reset();
			next_screen = ScreenInfo(SCREEN_EXPLANATION, language);
		}
	}
	void draw()
	{

	if( state == STATE_EXPLANATION )
	{
		explanationImg.draw(0,0);
		returnButton.display();
		return;
	}

		float Y = anim.update();

		bg.draw(0,0,ofGetWidth(), ofGetHeight());
		leaves_bottom.draw(0,0,ofGetWidth(), ofGetHeight());
		quadre.draw(0,0 - Y,ofGetWidth(), ofGetHeight());

	//	if( play_button[language].handOver()) 	ofSetColor(255,100);
	//else 						ofSetColor(255,255);
		play_button[language].display( ofVec2f(0.0,-Y) );

	//	if( lang_button[language].handOver()) 	ofSetColor(255,100);
	//else 						ofSetColor(255,255);
		lang_button[language].display(ofVec2f(0.0,-Y));
		ofSetColor(255);

		 ofxTrueTypeFontUC * usefont = &titlefont;
		 	if( language == LANG_RUS)
		 		usefont = &titlefontRUS;

		drawMultilineString(titleStr, ofVec2f(ofGetWidth()*0.5,  ofGetHeight()*0.4-Y), 600, *usefont, true, ofColor(255));


		leaves_top.draw(0,0,ofGetWidth(), ofGetHeight());

		helpButton.display();

	}
	



	void reset()
	{
		play_button[language].reset();
		lang_button[language].reset();
		returnButton.reset();
		ZeroTime();
		timeout = ofGetElapsedTimeMillis();
		//Tweens
		anim.setParameters(1,easingbounce,ofxTween::easeOut, 1200.0, 0.0, 2000.0, 0.0);
	}


	//GUIselectableImage play_button, lang_button;

	std::map<std::string, GUIselectableImage> play_button, lang_button;

	ofImage quadre;
	std::string titleStr;

};



#endif /* SCREENWELCOME_H_ */
