/*
 * ScreenLanguage.h
 *
 *  Created on: 25/02/2014
 *      Author: xaviersuaucuadros
 */

#ifndef SCREENBLABLALANGUAGE_H_
#define SCREENBLABLALANGUAGE_H_

#include "Screens.h"
#include "GUIelements.h"

class ScreenLanguage : public NatScreen{

public:

	ScreenLanguage( ) : NatScreen(){}
	~ScreenLanguage()
	{
		//std::cout << "Destroying Language" << std::endl;
	}

	void setup()
	{

		/*
				 * init common vars
				 */
				NatScreen::initCommonVars();

		leaves_bottom.loadImage("../../../common-assets/plantes_baix.png");
		leaves_top.loadImage("../../../common-assets/plantes_dalt_pinya.png");
		quadre.loadImage("../../../common-assets/lang/quadre.png");

		//Buttons
		/*back_button =  GUIselectableImage(0, 0, "CAT", "../../../common-assets/lang/boto-flexa.png", handAdaptor, true) ;
		back_button.setPos(ofGetWidth() * 490/1920.0, ofGetHeight() * 224/1080.0);
		back_button.setSize( 135,138 );
*/
		//Load language icons
		int deltax = ofGetWidth() * 0.4;
		for( int i=0; i<lang_vec.size(); i++)
		{
			icons[ lang_vec[i] ] =  GUIselectableImage(0, 0, lang_vec[i], "../../../common-assets/lang/" + lang_vec[i] + "_hover.png", handAdaptor, true) ;
			icons[ lang_vec[i] ].setHoverImage("../../../common-assets/lang/"+lang_vec[i]+".png");
			icons[ lang_vec[i] ].setPos( deltax - 200*0.5, ofGetHeight()*0.45 );
			icons[ lang_vec[i] ].setSize( 496,301 );
		}

		curr_language = 0;
		language = LANG_CAT;

		init_timer = ofGetElapsedTimeMillis();

		reset();
	}

	void load_language_data()
	{
		//now load that same file and get the values out
//		ofxXmlSettings settings;
//		settings.loadFile("strings.xml");
		std::map<std::string, GUIselectableImage>::iterator it = icons.begin();
		std::advance (it,curr_language);
//		langStr = settings.getValue(it->first + ":lang_title", "Selecciona el teu idioma");

		loadCommonStrings(language);

	}

	void update()
	{
		next_screen = ScreenInfo();

		//Updates the question and explanation buttons
		updateExplanation();
		if( state == STATE_EXPLANATION ) return;

		if( dyngest->swipe && CurrTime() > 200 ) //avoid consecutuve swipes
		{
			swipeSound.play();
			curr_language = (curr_language + 1) % icons.size() ;

			//Set the common var "language", better usage
			std::map<std::string, GUIselectableImage>::iterator it = icons.begin();
					std::advance (it,curr_language);
			language = it->first;

			load_language_data();
			ZeroTime();
			for( std::map<std::string, GUIselectableImage>::iterator it = icons.begin(); it!=icons.end(); it++ )
			{
				it->second.reset();
			}
		}

		if( ofGetElapsedTimeMillis() - init_timer < 2000 ) return;

		int count = 0;
		for( std::map<std::string, GUIselectableImage>::iterator it = icons.begin(); it!=icons.end(); it++, count++ )
		{

			 if( curr_language == count )
			 {
			 	it->second.updateSelectProgress();

			 	if( it->second.selected )
			 	{
			 		std::cout<<"selected language "<<it->first<<std::endl;
			 		it->second.reset();
			 		next_screen = ScreenInfo(SCREEN_EXPLANATION, it->first);
			 	}
			 }
			else
			{
				it->second.reset();
			}
		}

/*
		back_button.updateSelectProgress();
		if( back_button.selected )
		{
			back_button.reset();
			std::map<std::string, GUIselectableImage>::iterator it = icons.begin();
			std::advance (it,curr_language);
			next_screen = ScreenInfo(SCREEN_WELCOME, language);
		}
*/
	}

	void draw()
	{

			if( state == STATE_EXPLANATION )
	{
		explanationImg.draw(0,0);
		returnButton.display();
		return;
	}


		float Y = anim.update();
		bg.draw(0,0,ofGetWidth(), ofGetHeight());
		leaves_bottom.draw(0,0,ofGetWidth(), ofGetHeight());
		quadre.draw(0, 0 - Y,ofGetWidth(), ofGetHeight());



		ofxTrueTypeFontUC * usefont = &font;
			if( language == LANG_RUS)
				usefont = &fontRUS;

		drawMultilineString(langStr, ofVec2f(ofGetWidth()*0.5,  ofGetHeight()*425/1080.0 - Y), 600, *usefont, true, ofColor(255));

		std::map<std::string, GUIselectableImage>::iterator it = icons.begin();
		std::advance (it,curr_language);
		icons[language].display( ofVec2f(0.0,-Y) );

		//back_button.display( ofVec2f(0.0,-Y) );

		leaves_top.draw(0,0,ofGetWidth(), ofGetHeight());

				helpButton.display();


	}



	void reset()
	{
	
			returnButton.reset();
		timeout = ofGetElapsedTimeMillis();
		anim.setParameters(1,easingbounce,ofxTween::easeOut, 1200.0, 0.0, 2000.0, 0.0);
		for( std::map<std::string, GUIselectableImage>::iterator it = icons.begin(); it!=icons.end(); it++ )
		{
			it->second.reset();
			it->second.selected = false;
		}
		init_timer = ofGetElapsedTimeMillis();
		ZeroTime();
	}

	//void exit();
	int curr_language;
	ofImage quadre;
	std::map<std::string, GUIselectableImage> icons;
	GUIselectableImage back_button;

	long int init_timer;


};



#endif /* SCREENLANGUAGE_H_ */
