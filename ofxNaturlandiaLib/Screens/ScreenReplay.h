/*
 * SCREENREPLAY_H.h
 *
 *  Created on: 24/02/2014
 *      Author: xaviersuaucuadros
 */

#ifndef SCREENREPLAY_H_
#define SCREENREPLAY_H_

#include "Screens.h"
#include "GUIelements.h"

class ScreenReplay : public NatScreen{

public:

	ScreenReplay() : NatScreen(){}

	~ScreenReplay()
	{
		//std::cout << "Destroying replay" << std::endl;
	}

	void setup()
		{
			std::cout<<"replay setup"<<std::endl;



			bg.loadImage("common/bg_replay.png");


			ofTrueTypeFont::setGlobalDpi(72);
			font.loadFont("Arial.ttf", 30, true, true);
			font.setLineHeight(20.0f);
			font.setLetterSpacing(1.0);


		}
		void load_language_data()
		{
			//now load that same file and get the values out
			ofxXmlSettings settings;
			settings.loadFile("strings.xml");
			titleStr = settings.getValue(language + ":replay_title", "Torna-ho a intentar!");
		}

		void update()
		{
			next_screen = ScreenInfo();

			if( dyngest->swipe && CurrTime() > 200)
			{
				next_screen = ScreenInfo(SCREEN_GAME, language);
				ZeroTime();
			}
		}
		void draw()
		{
			ofSetColor(0);
			bg.draw(0,0,ofGetWidth(), ofGetHeight());
			ofSetColor(255);
			font.drawString(titleStr, ofGetWidth()*0.3,  ofGetHeight()*0.4  );
		}



		void reset()
		{
			timeout = ofGetElapsedTimeMillis();
		}

		//void exit();
		long timeout;
		ofImage bg;
		ofxTrueTypeFontUC	font;

	    std::string titleStr;

	};



#endif /* SCREENWELCOME_H_ */
