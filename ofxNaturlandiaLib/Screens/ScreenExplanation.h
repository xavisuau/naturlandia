/*
 * ScreenExplanation.h
 *
 *  Created on: 09/05/2014
 *      Author: xaviersuaucuadros
 */

#ifndef SCREENEXPLANATION_H_
#define SCREENEXPLANATION_H_



#include "Screens.h"
#include "GUIelements.h"

class ScreenExplanation : public NatScreen{

public:

	ScreenExplanation() : NatScreen(){}
	~ScreenExplanation()
	{
		//std::cout << "Destroying explanation" << std::endl;
	} 

	void setup()
	{
		/*
				 * init common vars
				 */
				NatScreen::initCommonVars();

					leaves_bottom.loadImage("../../../common-assets/plantes_baix.png");
					leaves_top.loadImage("../../../common-assets/plantes_dalt_pinya.png");


						anim2.setParameters(2,easingquad,ofxTween::easeOut, 0.0, 255.0, 1000.0, 1000.0);


		std::cout<<" done"<<std::endl;

	}
	void load_language_data()
	{
		//now load that same file and get the values out
		ofxXmlSettings settings;
		settings.loadFile("strings.xml");
		explanationStr = settings.getValue(language + ":explanation", "Mou la m� per jugar!");
	}

	void update()
	{

		next_screen = ScreenInfo();

		//Updates the question and explanation buttons
		updateExplanation();
		if( state == STATE_EXPLANATION ) return;

		if( dyngest->push && CurrTime() > 200)
		{
			next_screen = ScreenInfo(SCREEN_GAME, language);
			ZeroTime();
		}
	}
	void draw()
	{

			if( state == STATE_EXPLANATION )
	{
		explanationImg.draw(0,0);
		returnButton.display();
		return;
	}


		float scale = anim.update();
		float alpha = anim2.update();

		bg.draw(0,0,ofGetWidth(), ofGetHeight());
		leaves_bottom.draw(0,0,ofGetWidth(), ofGetHeight());
		leaves_top.draw(0,0,ofGetWidth(), ofGetHeight());
			splash.draw(ofGetWidth()*0.5, ofGetHeight()*0.5,ofGetWidth()*scale, ofGetHeight()*scale);
			ofSetColor(0);

			ofxTrueTypeFontUC * usefont = &font;
			if( language == LANG_RUS)
				usefont = &fontRUS;

//			usefont->drawString(cutStringToWidth(explanationStr, 500, *usefont), ofGetWidth()*0.3,  ofGetHeight()*0.4  );
			drawMultilineString(explanationStr, ofVec2f(ofGetWidth()*0.5,  ofGetHeight()*0.56), 500, *usefont, true, ofColor(0,alpha));

			ofSetColor(255);

					helpButton.display();

	}



	void reset()
	{
		timeout = ofGetElapsedTimeMillis();
		//Tweens
		anim.setParameters(1,easingelastic,ofxTween::easeOut, 0, 1.0, 1500.0, 0.0);
		anim2.setParameters(2,easingquad,ofxTween::easeOut, 0.0, 255.0, 1000.0, 1200.0);
		ZeroTime();
	}


	ofxTween anim2;


};






#endif /* SCREENEXPLANATION_H_ */
