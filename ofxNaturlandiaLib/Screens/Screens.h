#ifndef fezoo_Screens_h
#define fezoo_Screens_h


#ifndef NO_NITE
	#include "HandTrackerThread.h"
#endif

#include "ofMain.h"
#include "ofxTrueTypeFontUC.h"
#include "ofxTween.h"
#include "ofxXmlSettings.h"

#ifndef NO_NITE
#include "GUIelements.h"
#endif

#define SCREEN_WELCOME  0
#define SCREEN_LANGUAGE 1
#define SCREEN_GAME     2
#define SCREEN_SCORE    3
#define SCREEN_EXPLANATION  4

#define LANG_CAT		"CAT"
#define LANG_ESP		"ESP"
#define LANG_ENG		"ENG"
#define LANG_FRA		"FRA"
#define LANG_RUS		"RUS"


class LangVector : public std::vector<std::string>
{
public:
    LangVector()
    {
	  push_back(LANG_CAT);
	  push_back(LANG_ESP);
	  push_back(LANG_ENG);
	  push_back(LANG_FRA);
	  push_back(LANG_RUS);
    }
} static lang_vec;

const int STATE_PLAYING 		= 0;
const int STATE_REPLAY 			= 1;
const int STATE_WINNER 			= 2;
const int STATE_GOOD_ANSWER 	= 3;
const int STATE_EXPLANATION 	= 4;

#define MULTI_LINE_STRING(a) #a

struct ScreenInfo
{
	std::string language;
	int			code;
	ScreenInfo() : code(-1), language(LANG_CAT) {}
	ScreenInfo(int c, std::string l) : code(c), language(l) {}
};
#ifndef NO_NITE
class NatScreen
#else
class NatScreen : public ofBaseApp
#endif 
{
public:

#ifndef NO_NITE

	NatScreen()
{
		language = LANG_CAT; //default
		ZeroTime();
		show_hand = true;
}

virtual ~NatScreen()
{
	std::cout<<"Base Destructor"<<std::endl;
}

#endif

	virtual void setup()	{std::cout<<"base setup"<<std::endl;}
	virtual void update()	{std::cout<<"base update"<<std::endl;}
	virtual void draw()	{std::cout<<"base draw"<<std::endl;}
	virtual void load_language_data()	{std::cout<<"nothing to load"<<std::endl;}
	virtual void do_specific_things()	{std::cout<<"base specific"<<std::endl;}



	//
	ScreenInfo nextScreen(){ return next_screen; }


	void initCommonVars()
	{
		state = STATE_PLAYING;
		playWinnerSound = true;

		can_shuffle = false;




		bg.loadImage("../../../common-assets/fondo.png");
		leaves_bottom.loadImage("../../../common-assets/plantes_baix.png");
		leaves_top.loadImage("../../../common-assets/plantes_dalt.png");

#ifndef NO_NITE

		//Buttons
		menuButton =  GUIselectableImage(0, 0, "CAT", "../../../common-assets/boto-menu.png", handAdaptor, true) ;
		menuButton.setPos(ofGetWidth() * 238.0/1920.0, ofGetHeight() * 31.0/1080.0);
		menuButton.setSize( 224,89 );

		helpButton =  GUIselectableImage(0, 0, "CAT", "../../../common-assets/boto-interrogant.png", handAdaptor, true) ;
		helpButton.setPos(ofGetWidth() * 84.0/1920.0, ofGetHeight() * 15.0/1080.0);
		helpButton.setSize( 123,123 );

		returnButton =  GUIselectableImage(0, 0, "CAT", "../../../common-assets/boto-tornar.png", handAdaptor, true) ;
		returnButton.setPos(ofGetWidth() * 44.0/1920.0, ofGetHeight() * 15.0/1080.0);
#endif


		winner_bg.loadImage("../../../common-assets/pintura.png");
		winner_bg.setAnchorPercent(0.5,0.5);
		winner_icon.loadImage( "../../../common-assets/boto-genial.png");
		winner_icon.setAnchorPercent(0.5,0.5);
		replay_bg.loadImage("../../../common-assets/pintura.png");
		replay_bg.setAnchorPercent(0.5,0.5);
		replay_icon.loadImage("../../../common-assets/boto-torna-intentar.png");
		replay_icon.setAnchorPercent(0.5,0.5);

		explanationImg.loadImage("../../../common-assets/explicacio-joc.jpg");

		splash.loadImage("../../../common-assets/pintura.png");
		splash.setAnchorPercent(0.5,0.5);

		//Fonts
		titlefont.loadFont("../../../common-assets/GoodDog.otf", 100, true, true);
		titlefont.setLineHeight(120.0f);
		titlefont.setLetterSpacing(1.0);
		titlefontRUS.loadFont("../../../common-assets/verdana.ttf", 100, true, true);
		titlefontRUS.setLineHeight(120.0f);
		titlefontRUS.setLetterSpacing(1.0);

		font.loadFont("../../../common-assets/GoodDog.otf", 70, true, true);
		font.setLineHeight(100.0f);
		font.setLetterSpacing(1.0);
		fontRUS.loadFont("../../../common-assets/verdana.ttf", 70, true, true);
		fontRUS.setLineHeight(100.0f);
		fontRUS.setLetterSpacing(1.0);

		float maxSize = winner_bg.getHeight();
		winnerSize.setParameters(2,easingelastic,ofxTween::easeOut, 1, maxSize, 2000.0, 1000.0);

		correctSound.loadSound(  "../../../common-assets/correct.wav", true );
		wrongSound.loadSound(  "../../../common-assets/wrong.mp3", true );
		winnerSound.loadSound( "../../../common-assets/winnerSound.mp3", true );
		swipeSound.loadSound("../../../common-assets/swipe.wav", true);

		//Lower louder sounds
		correctSound.setVolume(0.2f);
		winnerSound.setVolume(0.3f);
	}

	void loadCommonStrings(std::string lang)
	{
		ofxXmlSettings settings;
		settings.loadFile("../../../common-assets/commonstrings.xml");
		replayStr = settings.getValue(lang + ":replay_title", "Torna-ho a provar!");
		winnerStr = settings.getValue(lang + ":winner_title", "Enhorabona!");
		langStr   = settings.getValue(lang + ":lang_title", "Idioma");
	}

#ifndef NO_NITE
	void setDynamics( DynamicGestures * _d)
	{
		dyngest = _d;
	}
#endif

	void setLanguage(std::string l)
	{
		language = l;
		load_language_data();
	}

	//Virtual method, must be implemented
	virtual void reset(){}


	void Tag(std::string tag)
	{
		if( !tags.count(tag) )
		{
			tags[tag] = 1;
		}
	}

	void UnTag(std::string tag)
	{
		if( tags.count(tag) )
		{
			tags.erase(tag);
		}
	}

	bool HasTag(std::string tag)
	{
		return tags.count(tag);
	}

	long int CurrTime()
	{
		return  ofGetElapsedTimeMillis() - time;
	}

	void ZeroTime()
	{
		time = ofGetElapsedTimeMillis();
	}

	std::string cutStringIntoLines(std::string str)
	{
		std::stringstream ss;
		std::size_t pos = 0;
		while( true )
		{
			std::size_t newpos = str.find("//", pos);
			ss<<str.substr(pos, newpos-pos )<<std::endl;
			if( newpos >= str.length() )
				break;

			pos = newpos+2;
		}
		return ss.str();
	}

	std::string cutStringToWidth(std::string str, int width, ofxTrueTypeFontUC & font)
	{
		std::stringstream ss;
		std::size_t pos = 0, lastpos = 0;
		while( true )
		{
			std::size_t spacepos = str.find(" ", pos);
			if( font.stringWidth( str.substr(lastpos, spacepos-lastpos) ) >= width || spacepos == std::string::npos )
			{
				//						std::cout<<"CUTTING: "<<str.substr(lastpos, spacepos-lastpos) << " at pos "<<spacepos<<std::endl;
				ss<<str.substr(lastpos, spacepos-lastpos) <<endl;
				lastpos = spacepos+1;
				pos = lastpos;
			}
			else
			{
				pos = spacepos+1;
			}


			if( spacepos == std::string::npos )
				break;
		}
		return ss.str();
	}


	std::vector<std::string> cutToStringVec(std::string str, int width, ofxTrueTypeFontUC & font)
	{
		std::size_t pos = 0, lastpos = 0;
				std::vector<std::string> lines;
				while( true )
				{
					std::size_t spacepos = str.find(" ", pos);
					int W = font.stringWidth( str.substr(lastpos, spacepos-lastpos) );
					if( W >= width || spacepos == std::string::npos )
					{
						//						std::cout<<"CUTTING: "<<str.substr(lastpos, spacepos-lastpos) << " at pos "<<spacepos<<std::endl;
						std::string line = str.substr(lastpos, spacepos-lastpos);

						lines.push_back(line);

						lastpos = spacepos+1;
						pos = lastpos;
					}
					else
					{
						pos = spacepos+1;
					}


					if( spacepos == std::string::npos )
						break;

				}

				return lines;
	}


	void drawStringVec(std::vector<std::string> & lines, ofVec2f textpos, int width, ofxTrueTypeFontUC & font, bool center = true,  ofColor color = ofColor(0,0,0))
	{
		int X,Y,W;
		int lineH = font.getLineHeight();

		ofSetColor(color);

		for(int i=0; i<lines.size(); i++)
			{

				 X = textpos.x;
				 Y = textpos.y + lineH * i;

				if( center )
				{
					W = font.stringWidth( lines[i] );
					X = textpos.x -  W * 0.5;
					Y = textpos.y + lineH * i - lineH * lines.size() * 0.5;
				}

				font.drawString( lines[i], X, Y );
			}

		ofSetColor(255);
	}

	void drawMultilineString(std::string str, ofVec2f textpos, int width, ofxTrueTypeFontUC & font, bool center = true,  ofColor color = ofColor(0,0,0))
	{
		std::size_t pos = 0, lastpos = 0;
		std::vector<std::string> lines;
		while( true )
		{
			std::size_t spacepos = str.find(" ", pos);
			int W = font.stringWidth( str.substr(lastpos, spacepos-lastpos) );
			if( W >= width || spacepos == std::string::npos )
			{
				//						std::cout<<"CUTTING: "<<str.substr(lastpos, spacepos-lastpos) << " at pos "<<spacepos<<std::endl;
				std::string line = str.substr(lastpos, spacepos-lastpos);

				lines.push_back(line);

				lastpos = spacepos+1;
				pos = lastpos;
			}
			else
			{
				pos = spacepos+1;
			}


			if( spacepos == std::string::npos )
				break;

		}

		for(int i=0; i<lines.size(); i++)
		{
			int W = font.stringWidth( lines[i] );

			int X = textpos.x;
			if(center)
				X = textpos.x -  W * 0.5;

			int Y = textpos.y + font.getLineHeight() * i;
			if( center )
				Y = textpos.y + font.getLineHeight() * i - font.getLineHeight() * lines.size() * 0.5;

			ofSetColor(color);
			font.drawString( lines[i], X, Y );
			ofSetColor(255);
		}

	}

#ifndef NO_NITE

void updateExplanation()
{
	if(state == STATE_EXPLANATION && CurrTime() > 1000 )
	{
		returnButton.updateSelectProgress();
		if( returnButton.selected )
		{
			ZeroTime();
			returnButton.reset();
			state = STATE_PLAYING;
			std::cout<<"Back"<<std::endl;
		}
	}
	else
	{
		returnButton.reset();
	}
	
	if( state != STATE_EXPLANATION && CurrTime() > 1000)
	{
		helpButton.updateSelectProgress();
		if( helpButton.selected )
		{
			ZeroTime();
			helpButton.reset();
			state = STATE_EXPLANATION;	
			std::cout<<"Forth"<<std::endl;
		}
	}
	else
	{
		helpButton.reset();
	}
}

#endif

	ScreenInfo next_screen;
	std::string language;
#ifndef NO_NITE
	HandPoseAdaptor * handAdaptor;
	DynamicGestures * dyngest;
#endif
	bool show_hand;


protected:
	/*
	 * Common game vars
	 */
	int state;
	bool canPlay, can_shuffle;

	ofImage  bg, leaves_top, leaves_bottom, splash;

#ifndef NO_NITE
	GUIselectableImage menuButton, helpButton, returnButton;
		PointsBar bar;
	LifeBar   heart;
#endif
	ofImage replay_bg, winner_bg, replay_icon, winner_icon, explanationImg;
	std::string replayStr, winnerStr, explanationStr, langStr;


	int life, points;


	/* Declare Fonts */
	ofxTrueTypeFontUC	titlefont, titlefontRUS, font, fontRUS;


	clock_t init, elapsed;
	long int timeout;

	//Tweens
	ofxTween helloBgPos;




	ofSoundPlayer correctSound, wrongSound,  winnerSound, swipeSound;
	bool playWinnerSound;


	std::map<std::string, int> tags;
	long int time;

	//Tweens
	ofxTween anim;
	ofxTween titleY, winnerSize;
	float animDuration;

	ofxEasingBack       easingback;
	ofxEasingBounce 	easingbounce;
	ofxEasingCirc       easingcirc;
	ofxEasingCubic      easingcubic;
	ofxEasingElastic    easingelastic;
	ofxEasingExpo       easingexpo;
	ofxEasingLinear 	easinglinear;
	ofxEasingQuad       easingquad;
	ofxEasingQuart      easingquart;
	ofxEasingQuint      easingquint;
	ofxEasingSine       easingsine;
};

///////

//class myScreen : public Screen
//{
//public:
//	myScreen() : Screen(){}
//
//	int nextWindow()
//	{
//		return 0;
//	}
//};

#endif
