//
//  serialUtils.h
//  jocNaturlandia
//
//  Created by Xavier Suau Cuadros on 25/10/13.
//
//

#ifndef jocNaturlandia_serialUtils_h
#define jocNaturlandia_serialUtils_h


class SerialUtils {


public:

SerialUtils(){}

void splitString( std::string str, char ch, std::vector<std::string> & fields)
{
    std::size_t pos = 0;
    std::size_t endpos = str.find('\n', pos);
    
    fields.clear();
    
    while (true) {
        std::size_t chpos = str.find(ch, pos);
        
        if (chpos!=std::string::npos)
        {
            std::string tmp = str.substr(pos, chpos-pos);
            fields.push_back(tmp);
            pos = chpos+1;
        }
        else
        {
            std::string tmp = str.substr(pos, endpos-pos);
            fields.push_back(tmp);
            break;
        }
    }
    
   // cout<<endl;

}

};

#endif
