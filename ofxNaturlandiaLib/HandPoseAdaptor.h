/*
 * DynamicGestures.h
 *
 *  Created on: 26/02/2014
 *      Author: xaviersuaucuadros
 */

#ifndef DYNAMICGESTURES_H_
#define DYNAMICGESTURES_H_

#include "HandProvider.h"
#include "ofMain.h"
#include <ctime>

///
/// A class with hand information and timestamp
///
struct HandStamp
{
	ofVec3f hand, uv_hand;
	clock_t		time;
	HandStamp()
	{
		hand = ofVec3f(0,0,0);
		time = -1;
	}
	HandStamp(ofVec3f & _h, ofVec3f & _uvh, clock_t _t)
	{
		hand = _h;
		uv_hand = _uvh;
		time = _t;
	}
};

///
/// A class with hand information and timestamp
///
class CircularHandBuffer
{
public:
	CircularHandBuffer(int _mn = 6) : maxN(_mn), N(0), pos(0)
{
		data.resize(maxN);
}

	void add(HandStamp  pt)
	{
		pos = (pos+1)%maxN;
		data[pos] = pt;
		N++;
	}

	void clear()
	{
		N=0;
		pos = 0;
		data.clear();
		data.resize(maxN);
	}

	int get_N()
	{
		return N;
	}

	int get_pos()
	{
		return pos;
	}

	float sign_of( float val )
	{
		if( val >= 0 ) return 1.0;
		else            return -1.0;
	}

	///
	/// get the difference between the most recent and oldest hands
	///
	HandStamp get_max_span()
	{
		HandStamp ret;

		if( N>=maxN )
		{
			ret.hand = data[pos].hand- data[(pos+1)%maxN].hand;
			ret.uv_hand = data[pos].uv_hand- data[(pos+1)%maxN].uv_hand;
			ret.time = data[pos].time- data[(pos+1)%maxN].time;
			//				std::cout<<"span "<<ret<<std::endl;
		}
		return ret;
	}


	///
	/// returns true if all the vectors (difference of consecutive hands x coordinates) are coherent with sign
	///
	bool coherent_x_movement(int sign = 1.0)
	{
		if( N<maxN ) return false;

		bool ret = true;
		for( int i=0; i<maxN-1; i++ )
		{
			if( sign_of(data[(pos+2+i)%maxN].hand.x - data[(pos+1+i)%maxN].hand.x) != sign )
			{
				ret = false;
				break;
			}
		}
		return ret;
	}

	///
		/// returns true if all the vectors (difference of consecutive hands x coordinates) are coherent with sign
		///
		bool coherent_push_movement()
		{
			if( N<maxN ) return false;

			bool ret = true;
			for( int i=0; i<maxN-1; i++ )
			{
				if( sign_of(data[(pos+2+i)%maxN].hand.z - data[(pos+1+i)%maxN].hand.z) != -1.0 )
				{
					ret = false;
					break;
				}
			}
			return ret;
		}
	///
	/// returns the average UV coordinates of a hand
	///
	ofVec3f avg_uv()
	{
		ofVec3f uv(0,0,0);
		for( int i=0; i<maxN; i++ )
		{
			uv = uv + data[(pos+2+i)%maxN].uv_hand;
		}
		return uv * (1.0 / (float)(maxN));
	}

	ofVec3f avg_hand()
	{
		ofVec3f avg(0,0,0);
		for( int i=0; i<maxN; i++ )
		{
			avg = avg + data[(pos+2+i)%maxN].hand;
		}
		return avg * (1.0 / (float)(maxN));
	}

	///
	/// returns the average speed of all the vectors (x coordinate)
	///
	double avg_x_speed()
	{
		double speed = 0;
		for( int i=0; i<maxN-1; i++ )
		{
			double dist = data[(pos+2+i)%maxN].hand.x - data[(pos+1+i)%maxN].hand.x ;
			double time = data[(pos+2+i)%maxN].time - data[(pos+1+i)%maxN].time;
			speed += dist / (time/CLOCKS_PER_SEC*1000.0);

		}
		return speed / (double)(maxN-1);
	}

	///
	/// returns the max speed amongst the vectors (x coordinate)
	///
	double max_x_speed(float sign)
	{
		double maxspeed = 0;
		for( int i=0; i<maxN-1; i++ )
		{
			double dist = data[(pos+2+i)%maxN].hand.x - data[(pos+1+i)%maxN].hand.x ;
			double time = data[(pos+2+i)%maxN].time - data[(pos+1+i)%maxN].time;
			double speed = dist / (time/CLOCKS_PER_SEC*1000.0);
			if( fabs(speed) > fabs(maxspeed) && sign_of(speed) == sign_of(sign) )
			{
				maxspeed = speed;
			}
		}
		return maxspeed;
	}

	double max_z_speed(float sign)
	{
		double maxspeed = 0;
		for( int i=0; i<maxN-1; i++ )
		{
			double dist = data[(pos+2+i)%maxN].hand.z - data[(pos+1+i)%maxN].hand.z ;
			double time = data[(pos+2+i)%maxN].time - data[(pos+1+i)%maxN].time;
			double speed = dist / (time/CLOCKS_PER_SEC*1000.0);
			if( fabs(speed) > fabs(maxspeed) && sign_of(speed) == sign_of(sign) )
			{
				maxspeed = speed;
			}
		}
		return maxspeed;
	}

private:
	int N, maxN, pos;
	std::vector<HandStamp> data;

};



///
/// struct containing flags for each gesture. Flags tell if that gesture was detected during the last frame.
///
struct DynamicGestures
{
	bool swipe, push;
	ofVec3f swipe_pos, push_pos;
	bool swipeL;
	bool swipeR;
	DynamicGestures() : swipe(false), swipeL(false), swipeR(false), push(false), swipe_pos(0,0,0)
	{}
};


///
/// A class to detect dynamic gestures from the hand in handProvider.
///
class HandPoseAdaptor
{
public:

	HandPoseAdaptor()
	{
	}

	~HandPoseAdaptor()
	{
	}

	///
	/// Constructor. Takes a pointer to a handProvider, and the length of the hand buffer
	///
	HandPoseAdaptor(HandProvider * hp, int buff_len)
	{
		handProvider = hp;
		hand_buffer = CircularHandBuffer(buff_len);
		push_buffer = CircularHandBuffer(buff_len);
		timestamp = clock();
		min_first_frames = 3;
		first_filter_frame = true;


//		KF = cv::KalmanFilter(6,2,0);
//		//KF.transitionMatrix = *(cv::Mat_<float>(4, 4) << 1,0,1,0, 0,1,0,1, 0,0,1,0, 0,0,0,1);
//		KF.transitionMatrix = *(cv::Mat_<float>(6, 6) << 1,0,1,0,0.5,0, 0,1,0,1,0,0.5, 0,0,1,0,1,0, 0,0,0,1,0,1, 0,0,0,0,1,0, 0,0,0,0,0,1);
//		KF.measurementMatrix = *(cv::Mat_<float>(2, 6) << 1,0,1,0,0.5,0, 0,1,0,1,0,0.5);
//
//		// init...
//		KF.statePre.at<float>(0) = 640/2;
//		KF.statePre.at<float>(1) = 480/2;
//		KF.statePre.at<float>(2) = 0;
//		KF.statePre.at<float>(3) = 0;
//		KF.statePre.at<float>(4) = 0;
//		KF.statePre.at<float>(5) = 0;
////		cv::setIdentity(KF.measurementMatrix);
//		cv::setIdentity(KF.processNoiseCov, cv::Scalar::all(1e-1)); 	 //mm --> confidence in model
//		cv::setIdentity(KF.measurementNoiseCov, cv::Scalar::all(100.0)); //mm --> confidence in measure (tracking)
//		cv::setIdentity(KF.errorCovPost, cv::Scalar::all(1000.0));
	}

	///
	///update the detector, to be called every frame
	///
	inline void update();

	///
	///returns the currently detected gestures
	///
	inline DynamicGestures getGestures();

	ofVec3f getFilteredHand()
	{
		return hand;
	}

	ofVec3f getHandScaled()
	{
//		float scaledX = ofMap(uv_hand_f.x, 0, 320, 0, ofGetWidth() );
//		float scaledY = ofMap(uv_hand_f.y, 0, 240, 0, ofGetHeight() );

		ofVec2f scaledHand = ofVec2f( uv_hand_f.x, uv_hand_f.y );
		scaleToScreen(scaledHand, hand.z );
		return ofVec3f(scaledHand.x, scaledHand.y, 0);


	}

	int numHands()
	{
		return handProvider->getNumHands();
	}

	ofVec3f hand, uv_hand, uv_hand_f;
private:
	HandProvider * handProvider;
	DynamicGestures gestures;
	CircularHandBuffer hand_buffer, push_buffer;

	int num_hands, min_first_frames, last_num_hands, count_first_frames, hand_ID ;

	ofVec3f  last_hand, raw_hand, last_raw_hand;

	bool first_filter_frame;

	float hor_delta;

	clock_t timestamp;

	inline void filter_hand( ofVec3f & raw, ofVec3f & filtered, float filter);
	inline void updateHand( float filter = 1.0 );
	inline void updateSwipe( bool left_direction = true);
	inline void updatePush();
	inline float sign_of( float val );
    inline void scaleToScreen( ofVec2f & h, double d );

};

#include "HandPoseAdaptor.cpp"

#endif /* DYNAMICGESTURES_H_ */
