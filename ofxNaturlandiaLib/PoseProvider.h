#ifndef fezoo_poseProvider_h
#define fezoo_poseProvider_h

#include "NiTE.h"


#define MAX_USERS 10

#define USER_MESSAGE(msg) \
	{printf("[%08llu] User #%d:\t%s\n",ts, user.getId(),msg);}

///
///
/// \brief Pose Provider provides joint information of several users
///
/// It initializes tracking when people moves at a certain distance from the camera.
/// Methods are inline to avoid the "multiple definition" linking error
///

class PoseProvider
{
public:

	PoseProvider()
	{
		for (int i=0; i < MAX_USERS; ++i)
		{
			g_visibleUsers[i]=false;
			g_skeletonStates[i] = nite::SKELETON_NONE;
		}
	}

	~PoseProvider()
	{

	}

	/// Init client
	inline void init();

	/// Main method, to be called within a loop
	///
	/// Reads frame detects gesture and tracks whenever it has been initialized
	inline void process();

	/// Stop client
	inline void stop();

	/// Get number of tracked people
	inline int getNumUsers();

	/// Get identifier of the i-th hand
	inline short int getUserId( int i);

	/// Get X position (world coordinates) of the i-th hand
	inline double getHeadX( int i);

	/// Get Y position (world coordinates) of the i-th hand
	inline double getHeadY( int i);

	/// Get Z position (world coordinates) of the i-th hand
	inline double getHeadZ( int i);

	/// Get X position (in the image plane) of the i-th hand
	inline double getHeadU( int i);

	/// Get Y position (in the image plane) of the i-th hand
	inline double getHeadV( int i);


private:

	nite::UserTracker userTracker;
	nite::Status niteRc;
	nite::UserTrackerFrameRef userTrackerFrame;

	bool g_visibleUsers[MAX_USERS]; //= {false};
	nite::SkeletonState g_skeletonStates[MAX_USERS];// = {nite::SKELETON_NONE};

	inline void _updateUserState(const nite::UserData& user, unsigned long long ts);

};

#include "PoseProvider.cpp" //Header only

#endif
