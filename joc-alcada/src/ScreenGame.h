/*
 * ScreenGame.h
 *
 *  Created on: 24/02/2014
 *      Author: xaviersuaucuadros
 */

#ifndef SCREENGAME_H_
#define SCREENGAME_H_

#include "PoseProvider.h"

#include "GUIelements.h"
#include "question.h"
#include "ofxTween.h"
#include "ofxTrueTypeFontUC.h"
#include "ofxXmlSettings.h"

#include <ctime>

#define SCREEN_WELCOME  0
#define SCREEN_LANGUAGE 1
#define SCREEN_GAME     2
#define SCREEN_SCORE    3
#define SCREEN_EXPLANATION  4

#define LANG_CAT		"CAT"
#define LANG_ESP		"ESP"
#define LANG_ENG		"ENG"
#define LANG_FRA		"FRA"
#define LANG_RUS		"RUS"



const int STATE_PLAYING = 0;
const int STATE_REPLAY 	= 1;
const int STATE_WINNER 	= 2;
const int STATE_GOOD_ANSWER 	= 3;

const int INIT_LIFE = 3;
const int WIN_POINTS = 4;


class ScreenGame : public ofBaseApp{
    
public:

    ~ScreenGame(){}

    void setup();
    void update();
    void draw();
	void load_language_data();
	void reset();

    

    
    bool haveWinner();

    
    
    /*
    		 * Common game vars
    		 */
    			int state;
    			bool canPlay, can_shuffle;

    		    ofImage  bg, leaves_top, leaves_bottom;
    		    GUIselectableImage menuButton, helpButton;

    		    ofImage replay_bg, winner_bg, replay_icon, winner_icon;
    		    std::string replayStr, winnerStr;


    			int life, points;
    		    PointsBar bar;
    		    LifeBar   heart;

    			/* Declare Fonts */
    			ofxTrueTypeFontUC	font, fontRUS;


    			clock_t init, elapsed;
    			long int timeout;

    			//Tweens
    			ofxTween titleY, winnerSize, helloBgPos;

    			ofxEasingBack       easingback;
    			ofxEasingBounce 	easingbounce;
    			ofxEasingCirc       easingcirc;
    			ofxEasingCubic      easingcubic;
    			ofxEasingElastic    easingelastic;
    			ofxEasingExpo       easingexpo;
    			ofxEasingLinear 	easinglinear;
    			ofxEasingQuad       easingquad;
    			ofxEasingQuart      easingquart;
    			ofxEasingQuint      easingquint;
    			ofxEasingSine       easingsine;


    			ofSoundPlayer correctSound, wrongSound,  winnerSound;
    			bool playWinnerSound;

    			std::string language;

    			/**
    			 * Specific game vars
    			 */

    			PoseProvider pose;
    			ofVideoPlayer video;
    

};

#endif /* SCREENGAME_H_ */
