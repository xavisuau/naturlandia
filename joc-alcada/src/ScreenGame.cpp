/*
 * ScreenGame.cpp
 *
 *  Created on: 24/02/2014
 *      Author: xaviersuaucuadros
 */


#include "ScreenGame.h"

//--------------------------------------------------------------
void ScreenGame::setup(){


	/*
	 * init common vars
	 */
	state = STATE_PLAYING;
	playWinnerSound = true;

	can_shuffle = false;


	life = INIT_LIFE;
	points = 0;
	heart.setLife(life);

	bg.loadImage("../../../common-assets/fondo.png");
	leaves_bottom.loadImage("../../../common-assets/plantes_baix.png");
	leaves_top.loadImage("../../../common-assets/plantes_dalt.png");



	winner_bg.loadImage("../../../common-assets/pintura.png");
	winner_bg.setAnchorPercent(0.5,0.5);
	winner_icon.loadImage( "../../../common-assets/boto-genial.png");
	winner_icon.setAnchorPercent(0.5,0.5);
	replay_bg.loadImage("../../../common-assets/pintura.png");
	replay_bg.setAnchorPercent(0.5,0.5);
	replay_icon.loadImage("../../../common-assets/boto-torna-intentar.png");
	replay_icon.setAnchorPercent(0.5,0.5);


	//Fonts
	font.loadFont("../../../common-assets/GoodDog.otf", 70, true, true);
	font.setLineHeight(20.0f);
	font.setLetterSpacing(1.0);
	fontRUS.loadFont("../../../common-assets/verdana.ttf", 70, true, true);
	fontRUS.setLineHeight(20.0f);
	fontRUS.setLetterSpacing(1.0);

	float maxSize = winner_bg.getHeight();
	winnerSize.setParameters(2,easingelastic,ofxTween::easeOut, 1, maxSize, 2000.0, 1000.0);

	correctSound.loadSound(  "../../../common-assets/correct.wav", true );
	wrongSound.loadSound(  "../../../common-assets/wrong.mp3", true );
	winnerSound.loadSound( "../../../common-assets/winnerSound.mp3", true );


	/*
	 * init specific vars
	 */
	pose.init();


	video.loadMovie("game/video.avi");

	ofSetVerticalSync(true);
	ofSetFrameRate(60);
	ofBackground(255,255,255);

}

void ScreenGame::load_language_data()
{
	ofxXmlSettings settings;
			settings.loadFile("../../../common-assets/commonstrings.xml");
			replayStr = settings.getValue(language + ":replay_title", "Torna-ho a provar!");
			winnerStr = settings.getValue(language + ":winner_title", "Enhorabona!");
}


//--------------------------------------------------------------
void ScreenGame::update(){

video.update();
//	if( !video.isPlaying() )
//		video.play();

	if( !haveWinner() && state == STATE_PLAYING )
	{
		//TODO: Game things
		pose.process();
		if( pose.getNumUsers() > 0 )
		{
			std::cout<<"HEAD AT: "<<pose.getHeadX(0)<<", "<<pose.getHeadY(0)<<", "<<pose.getHeadZ(0)<<std::endl;

			float pctg = ofMap( pose.getHeadZ(0), 1000.0, 3000.0, 0.0, 1.0);
			video.setPosition(pctg);

		}
	}


}

//--------------------------------------------------------------
void ScreenGame::draw(){



	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glDisable(GL_DEPTH_TEST);

	//    ofEnableAlphaBlending();



	ofSetColor(255,255);
		bg.draw(0,0,ofGetWidth(), ofGetHeight());
		leaves_bottom.draw(0,0,ofGetWidth(), ofGetHeight());
		leaves_top.draw(0,0,ofGetWidth(), ofGetHeight());
		ofSetColor(255);

		video.draw(250,100);

}



void ScreenGame::reset()
{
	playWinnerSound = true;
	life = INIT_LIFE;
	points = 0;
}

bool ScreenGame::haveWinner()
{
	//TODO
	return false;
}


