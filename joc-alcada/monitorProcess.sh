#!/bin/bash



#Config environment
if [ "$#" -eq 1 ]
then
    searchFolder=$1
else
    searchFolder="/home/fezoo/"
fi



echo "Searching OpenNI in " $searchFolder  " ... "
openniENVFile=$(find $searchFolder -name OpenNIDevEnvironment 2> /dev/null)
echo " found environemnt file $openniENVFile"
source $openniENVFile

echo "Searching NiTE in " $searchFolder  " ... "
niteENVFile=$(find $searchFolder -name NiTEDevEnvironment 2> /dev/null)
echo " found environemnt file $niteENVFile"
source $niteENVFile

#-print -quit ensure that we get only the first result
echo "Searching OpenFrameworks in " $searchFolder  " ... "
OF_FOLDER=$(find $searchFolder -name OpenFrameworks -print -quit 2> /dev/null)
if [ !-z $OF_FOLDER ]
then
    echo "Searching under the name openFrameworks ..."
    OF_FOLDER=$(find $searchFolder -name openFrameworks -print -quit 2> /dev/null)
fi

echo " found OpenFrameworks : $OF_FOLDER"

export OF_ROOT=$OF_FOLDER

if [ $(uname) == "Linux" ]
then
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$NITE2_REDIST64:$OPENNI2_REDIST
    #Clone any addon you need
else
# [per MAC és]
        export DYLD_LIBRARY_PATH=$NITE2_REDIST64:$OPENNI2_REDIST
fi

# RUN
(cd /home/fezoo/openFrameworks/apps/naturlandia/joc-alcada/bin && ./joc-alcada)


