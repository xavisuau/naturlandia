#!/bin/bash

#  ofxXmlSettings should be already in addons
cwd=$(pwd)
cd ../../addons


if [ ! -d "ofxTween" ]; then
	echo "*** cloning ofxTween addon"
	git clone https://github.com/arturoc/ofxTween.git
else
	echo "*** updating ofxTween"
	git fetch https://github.com/arturoc/ofxTween.git 
fi

if [ ! -d "ofxCsv" ]; then
	echo "*** cloning ofxCsv addon"
	git clone https://github.com/WrongEntertainment/ofxCsv.git
else
	echo "*** updating ofxCsv"
	git fetch https://github.com/WrongEntertainment/ofxCsv.git 
fi



echo "Please get ofxSerial from: https://github.com/Doodle3D/Doodle3D/tree/master/addons/ofxSerial"
echo "ask xavi.suau@fezoo.cat"

echo " "
echo "Ask xavi.suau@fezoo.cat, a tweek has been made to compile https://github.com/hironishihara/ofxTrueTypeFontUC.git in Linux (problem with std11)"

 if [ ! -d "$ofxTrueTypeFontUC" ]; then
 	git fetch https://github.com/hironishihara/ofxTrueTypeFontUC.git 
 else
 	git clone https://github.com/hironishihara/ofxTrueTypeFontUC.git
 fi


echo "*** returning to $cwd"
cd $cwd
