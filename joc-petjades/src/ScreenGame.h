/*
 * ScreenGame.h
 *
 *  Created on: 24/02/2014
 *      Author: xaviersuaucuadros
 */

#ifndef SCREENGAME_H_
#define SCREENGAME_H_

#include "Screens.h"

#include "ofxXmlSettings.h"
#include "GUIelements.h"
#include "question.h"
#include "ofxTween.h"
#include "ofxSerial.h"
#include "serialUtils.h"

#include <ctime>


const int INIT_LIFE = 3;
const int WIN_POINTS = 4;



class ScreenGame : public NatScreen{

public:
    ScreenGame() : NatScreen(){}
    ~ScreenGame()
    {        
        std::cout << "Petjades ScreenGame destructor\n " << std::flush;
        std::cout<<" --- Closing serial port"<<std::endl; 
        serial.close();
    }

    void setup();
    void update();
    void draw();
	void load_language_data();
	void reset();


	void shuffle();

    void next_question();
    bool haveWinner();





    			/**
    			 * Specific game vars
    			 */
    			ofImage quadre;
    			std::vector<std::string> names;
    		    std::vector<Petjada> petjades;
    		    int currentQuestion;

    		    Petjada pressureButton;
    		    int buttonSensor;

    		    std::map<int, int> sensorMap;

    		    /*Serial vars*/
    		    ofxSerial serial;
    		    std::string readFromSerial;
    		    SerialUtils serialUtils;
    		    std::vector<float> pressure;


};

#endif /* SCREENGAME_H_ */
