/*
 * ScreenGame.cpp
 *
 *  Created on: 24/02/2014
 *      Author: xaviersuaucuadros
 */


#include "ScreenGame.h"

//--------------------------------------------------------------
void ScreenGame::setup(){


	/*
	 * init common vars
	 */
	NatScreen::initCommonVars();

		life = INIT_LIFE;
		points = 0;
		heart.setLife(life);
		animDuration = 1000.0;

		leaves_bottom.loadImage("../../../common-assets/plantes_baix.png");
		leaves_top.loadImage("../../../common-assets/plantes_dalt.png");

		show_hand = false;

	/*
	 * init specific vars
	 */

	/// LOAD IMAGES
	quadre.loadImage("game/quadre.png");

	names.push_back("ardilla");
//	names.push_back("ciervo");
	names.push_back("jabali");
	// add more...

	/*
	 * Sensor Map
	 *
	 * sensorMap[INDEX_OF_PETJADA_NAME] = INDEX_OF_SENSOR
	 *
	 */
	sensorMap[0] = 2;
//	sensorMap[1] = 1;
	sensorMap[1] = 0;
	buttonSensor = 1; // <-- special case, button sensor


	pressure = std::vector<float>( names.size() );
	currentQuestion = 0;
	petjades = std::vector<Petjada>( names.size() );
	for( int i=0; i<names.size(); i++ )
	{
		petjades[i] =  Petjada( "game/imatges/" + names[i] + ".png", 462, 197 ) ;
	}

	/*
	 * A button activated by pressure! I use the MENU graphics
	 */
	pressureButton =  Petjada( "../../../common-assets/boto-menu.png", ofGetWidth() * 238.0/1920.0, ofGetHeight() * 31.0/1080.0 ) ;

	//Set serial connection

	int baud = 9600;
	#ifdef linux
	std::cout<<"Searching serial /dev/ttyACM0"<<std::endl;
		serial.setup( "/dev/ttyACM0", baud ); //Change depending on computer
#else
		serial.setup( "/dev/tty.usbmodemfa141", baud ); //Change depending on computer
#endif




	ofSetVerticalSync(true);
	ofSetFrameRate(60);
	ofBackground(255,255,255);

}

void ScreenGame::load_language_data()
{
	loadCommonStrings(language);

	//now load that same file and get the values out
	ofxXmlSettings settings;
	settings.loadFile("strings.xml");
	explanationStr = settings.getValue(language + ":explanation", "Mou la mà per jugar!");

}


//--------------------------------------------------------------
void ScreenGame::update(){



	next_screen = ScreenInfo();



//	menuButton.updateSelectProgress();
//	if( menuButton.selected )
//	{
//		menuButton.reset();
//		next_screen = ScreenInfo(SCREEN_WELCOME, language);
//	}
//
//	helpButton.updateSelectProgress();
//	if( helpButton.selected )
//	{
//		helpButton.reset();
//		next_screen = ScreenInfo(SCREEN_EXPLANATION, language);
//	}

	/* read new data from serial */
		pressure.clear();
	    std::string newData = serial.readLine( '\n' );

	    // std::cout<<newData<<std::endl;

	    vector<std::string> fields;
	    if( newData != "" )
	    {
	       readFromSerial = newData;

	       serialUtils.splitString(readFromSerial,'\t',fields);


	// std::cout<<" got "<<fields.size()<<" fields"<<std::endl;

	        //Convert pressure to linear scale, since the sensor delivers log-scaled pressure. Està fet una mica a ull...
	//        pressure.clear();
	        for (int i=0; i<fields.size(); i++) {

	            //if (i>=names.size()) { break; }
	            pressure.push_back( ofMap(atof( fields[i].c_str() ), 0, 1024, 0, 100) );// 10*exp( 3*(  atof( fields[i].c_str() ) ) / 1024.0 ) - 10 );
	        }
	    }


	if( !haveWinner() && state == STATE_PLAYING && CurrTime() > 1500 )
	{

	    
// std::cout<<" got "<<pressure.size()<<" measurements"<<std::endl;

	    for( int i=0; i<petjades.size(); i++ )
	    {
	    	// std::cout<<"--- "<<currentQuestion<<" --- updating "<<i<<" with "<<sensorMap[i]<<" ("<<pressure[sensorMap[i]]<<")"<<std::endl;
	    	petjades[i].update( pressure[ sensorMap[i] ] );

	    	if( petjades[i].selected && i == currentQuestion )
	    	{
				correctSound.play();
				bar.setPoints(points+1);
				points++;
				next_question();
				//Load answer bear info
			//	state = STATE_GOOD_ANSWER;
				cout<<"YEAH"<<endl;
	    	}

	    	if( petjades[i].selected && i != currentQuestion )
	    	{
	    		cout<<"WRONG"<<endl;
	    		wrongSound.play();
	    		petjades[i].reset();
	    		ZeroTime();
	    		life--;
	    		heart.setLife(life);
	    		if( life == 0 )
	    		{
	    			state = STATE_REPLAY;
	    			return;
	    		}
	    	}

	    }



	}


	    pressureButton.update( pressure[buttonSensor] );
	    if( pressureButton.selected && CurrTime() > 2500)
	    {
	    ZeroTime();
	    /*
		if( state == STATE_GOOD_ANSWER && !haveWinner() )
		{
			state = STATE_PLAYING;
			next_question();
			points++;
		}
		else
		{
			next_screen = ScreenInfo(SCREEN_WELCOME, language);
		}
*/
					next_screen = ScreenInfo(SCREEN_WELCOME, language);

	    	pressureButton.reset();
	    	}

	/*
	 * Play again after winning or losing, trigger using a swipe
	 */
	if(  (  state == STATE_REPLAY || state == STATE_WINNER || state == STATE_GOOD_ANSWER ) &&
			dyngest->push 			&&
			CurrTime() > 200 )
	{

		if( state == STATE_GOOD_ANSWER )
		{
			// state = STATE_PLAYING;
			// next_question();
			// points++;
			//return;
		}

		swipeSound.play();
		ZeroTime();
		
		next_screen = ScreenInfo(SCREEN_WELCOME, language);

		reset();
	}
}

//--------------------------------------------------------------
void ScreenGame::draw(){



	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glDisable(GL_DEPTH_TEST);

	//    ofEnableAlphaBlending();



	ofSetColor(255,255);
		bg.draw(0,0,ofGetWidth(), ofGetHeight());
		leaves_bottom.draw(0,0,ofGetWidth(), ofGetHeight());
		leaves_top.draw(0,0,ofGetWidth(), ofGetHeight());
		ofSetColor(255);



		quadre.draw(0,0);

		petjades[currentQuestion].draw();
		if( CurrTime()>1500)
		{
	    for( int i=0; i<petjades.size(); i++ )
	    {
	    	petjades[i].drawProgress();
	    }
}



		ofxTrueTypeFontUC * usefont = &font;
			if( language == LANG_RUS)
				usefont = &fontRUS;


			ofSetColor(255);





	if( state == STATE_WINNER )
	{
		float imgSize = winnerSize.update();
		winner_bg.draw(ofGetWidth()/2, ofGetHeight()/2, ofGetWidth()*imgSize, ofGetHeight()*imgSize);
		ofSetColor(0);
//				usefont->drawString( winnerStr, ofGetWidth()*0.3,  ofGetHeight()*0.4  );

				drawMultilineString(winnerStr, ofVec2f(ofGetWidth()*0.5,  ofGetHeight()*0.5), 400, *usefont, true);

				ofSetColor(255);
				winner_icon.draw(ofGetWidth()/2, ofGetHeight()*0.65);


		if( playWinnerSound && !winnerSound.getIsPlaying() )
		{
			winnerSound.play();
			playWinnerSound = false;
		}
	}

	if( state == STATE_REPLAY )
	{
		float replaySize = winnerSize.update();
		replay_bg.draw(ofGetWidth()/2, ofGetHeight()/2, ofGetWidth()*replaySize, ofGetHeight()*replaySize);
		ofSetColor(0);
//		usefont->drawString( replayStr, ofGetWidth()*0.3,  ofGetHeight()*0.4  );
		drawMultilineString(replayStr, ofVec2f(ofGetWidth()*0.5,  ofGetHeight()*0.5), 650, *usefont, true);

		ofSetColor(255);
		replay_icon.draw(ofGetWidth()/2, ofGetHeight()*0.65);
	}


			if( state == STATE_GOOD_ANSWER )
				{
					int X=550;
					int Y=290;
					quadre.draw(0,0);

					ofSetColor(0);
									usefont->drawString( "Molt bé!", ofGetWidth()*0.3,  ofGetHeight()*0.4  );
									ofSetColor(255);
				}



	bar.display( ofVec2f(1668,519) );
	heart.display( ofVec2f(1678,225) );
			pressureButton.draw();
			if(CurrTime()>2500)
				pressureButton.drawProgress();
//		menuButton.display();
//	helpButton.display();

}

void ScreenGame::next_question()
{
	currentQuestion = (currentQuestion+1)%petjades.size();
	for( int i=0; i<petjades.size(); i++)
	{
		petjades[ i ].reset();
	}
}

void ScreenGame::reset()
{
	state = STATE_PLAYING;
	playWinnerSound = true;
	currentQuestion = (currentQuestion+1)%petjades.size();
	life = INIT_LIFE;
	points = 0;
	bar.setPoints(0);
	heart.setLife(INIT_LIFE);
	ZeroTime();
	for( int i=0; i<petjades.size(); i++)
	{
		petjades[ i ].reset();
	}

	anim.setParameters(1,easingbounce,ofxTween::easeOut, 1200.0, 0.0, 2000.0, 0.0);
	winnerSize.setParameters(2,easingelastic,ofxTween::easeOut, 0.0, 1.0, 2000.0, 1500.0);

}

bool ScreenGame::haveWinner()
{
	//TODO
	if( points == petjades.size() )
	{
		state = STATE_WINNER;
		return true;
	}
	else
		return false;
}


