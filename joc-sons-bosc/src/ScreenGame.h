/*
 * ScreenGame.h
 *
 *  Created on: 24/02/2014
 *      Author: xaviersuaucuadros
 */

#ifndef SCREENGAME_H_
#define SCREENGAME_H_

#include "Screens.h"

#include "ofxXmlSettings.h"
#include "GUIelements.h"
#include "question.h"
#include "ofxTween.h"


const int INIT_LIFE = 3;
const int WIN_POINTS = 4;


class ScreenGame : public NatScreen{

public:

	ScreenGame() : NatScreen(){}
    ~ScreenGame()
    {
    }
	void setup();
	void update();
	void draw();
	void load_language_data();
	void reset();
	void do_specific_things();

	void keyPressed  (int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y );
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);

	void shuffleZones();
	void resetZones();
	bool haveWinner();

	void exit();



//
//	/*
//	 * Common game vars
//	 */
//	ofImage  bg, leaves_top, leaves_bottom;
//	GUIselectableImage menuButton, helpButton;
//
//	bool canPlay, can_shuffle;
//	int result;
//
//	int state;
//
//	ofImage replay_bg, winner_bg, replay_icon, winner_icon;
//	std::string replayStr, winnerStr;
//
//	int life, points;
//	PointsBar bar;
//	LifeBar   heart;
//
//	/* Declare Fonts */
//	ofxTrueTypeFontUC	font, fontRUS;
//
//
//	clock_t init, elapsed;
//	long int timeout;
//
//	//Tweens
//	ofxTween titleY, winnerSize, helloBgPos;
//
//	ofxEasingBack       easingback;
//	ofxEasingBounce 	easingbounce;
//	ofxEasingCirc       easingcirc;
//	ofxEasingCubic      easingcubic;
//	ofxEasingElastic    easingelastic;
//	ofxEasingExpo       easingexpo;
//	ofxEasingLinear 	easinglinear;
//	ofxEasingQuad       easingquad;
//	ofxEasingQuart      easingquart;
//	ofxEasingQuint      easingquint;
//	ofxEasingSine       easingsine;
//
//	ofSoundPlayer correctSound, wrongSound,  winnerSound;
//	bool playWinnerSound;


	/**
	 * Specific game vars
	 */


	vector<std::string> iconNames;
	vector< GUIselectableImage >  answerIcons;
	vector< GUIselectableImage* > pAnswerIcons;
	vector< ofImage> frames;

	vector< SoundQuestion >     soundZones;
	vector< SoundConf >         soundConf;
	int currentAnswer;

	std::map<std::string,int> usedSounds;



	std::vector<SoundQuestion> questions;
	int currentQuestion;

	std::map< int, ofxTween > animX, animY, animH, animW;
	std::vector< ofVec2f > animPosIn, animPosOut;
	float animDuration;

};

#endif /* SCREENGAME_H_ */
