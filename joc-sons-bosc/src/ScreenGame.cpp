/*
 * ScreenGame.cpp
 *
 *  Created on: 24/02/2014
 *      Author: xaviersuaucuadros
 */


#include "ScreenGame.h"

const int FRAME_W = 631;
const int FRAME_H = 490;

//--------------------------------------------------------------
void ScreenGame::setup(){

	/*
	 * init common vars
	 */
	NatScreen::initCommonVars();

		life = INIT_LIFE;
		points = 0;
		heart.setLife(life);
		animDuration = 1000.0;

		leaves_bottom.loadImage("../../../common-assets/plantes_baix.png");
		leaves_top.loadImage("../../../common-assets/plantes_dalt.png");

	/*
	 * init specific vars
	 */
	animPosIn.push_back( ofVec2f( 289, 123 )  );
	animPosIn.push_back( ofVec2f( 990, 108 )  );
	animPosIn.push_back( ofVec2f( 280, 526 )  );
	animPosIn.push_back( ofVec2f( 952, 559 )  );

	animPosOut.push_back( ofVec2f( -ofGetWidth()*0.5, ofGetHeight()*0.0 )  );
	animPosOut.push_back( ofVec2f(  ofGetWidth()*1.5, ofGetHeight()*0.0 )  );
	animPosOut.push_back( ofVec2f( -ofGetWidth()*0.5, ofGetHeight()*0.5 )  );
	animPosOut.push_back( ofVec2f(  ofGetWidth()*1.5, ofGetHeight()*0.5 )  );

	frames.resize(4);
	frames[0].loadImage("game/quadre1.png") ;
	frames[1].loadImage("game/quadre2.png") ;
	frames[2].loadImage("game/quadre3.png") ;
	frames[3].loadImage("game/quadre4.png") ;

	currentQuestion = 0;


/*
 * ADD HERE ALL THE ANIMALS IN THE GAME
 */
	iconNames.push_back( "ciervo"       );
	iconNames.push_back( "lobo"         );
	iconNames.push_back( "abeja"       );
	iconNames.push_back( "marmota"      );
	iconNames.push_back( "oso"           );
	iconNames.push_back( "picot_negre"  );
	iconNames.push_back( "jabali" );
	iconNames.push_back( "rio"          );
	iconNames.push_back( "aguila"     );
	iconNames.push_back( "buho"     );
	iconNames.push_back( "gall_bosc"     );
	iconNames.push_back( "grillo"     );
	iconNames.push_back( "lince"     );
	iconNames.push_back( "rana"     );
	iconNames.push_back( "isard"     );



	std::cout<<"loading animals"<<endl;



	//Load Animals
	soundConf = vector< SoundConf >( iconNames.size() );
	for( int i=0;  i<iconNames.size(); i++ )
	{
//		std::stringstream ss;
//		ss<<"game/icons/"<<iconNames[i]<<".png";
//		soundConf[i].img.loadImage( ss.str() );

		/// Save icons for every orientation
		soundConf[i].images = std::vector<ofImage>(4);
		for( int j=0; j<4; j++)
		{
			std::stringstream ss2;
			ss2<<"game/animals/" << iconNames[i] << "/" << iconNames[i] << "-" << j+1 <<".png";
			soundConf[i].images[j].loadImage( ss2.str() );
		}


		std::stringstream ss_song;
		ss_song<<"game/sounds/"<<iconNames[i]<<".mp3";
		cout<<"loading sound "<<ss_song.str()<<endl;



		ofSoundPlayer newsong;
		newsong.loadSound( ss_song.str(), true ) ;
		soundConf[i].sound = newsong;

		if( iconNames[i].compare( "tormenta" ) == 0 ) soundConf[i].sound.setVolume(0.3f);

		soundConf[i].name = iconNames[i];
	}


	//Create soundZones
	soundZones.push_back( SoundQuestion( animPosOut[0], ofVec2f(FRAME_W,FRAME_H), &(soundConf[0].sound), &(soundConf[0].images[0]), handAdaptor, false) );
	soundZones.push_back( SoundQuestion( animPosOut[1], ofVec2f(FRAME_W,FRAME_H), &(soundConf[1].sound), &(soundConf[1].images[1]), handAdaptor, false) );
	soundZones.push_back( SoundQuestion( animPosOut[2], ofVec2f(FRAME_W,FRAME_H), &(soundConf[2].sound), &(soundConf[2].images[2]), handAdaptor, false) );
	soundZones.push_back( SoundQuestion( animPosOut[3], ofVec2f(FRAME_W,FRAME_H), &(soundConf[3].sound), &(soundConf[3].images[3]), handAdaptor, false) );

	for(int i=0; i<soundZones.size(); i++ )
	{
		//        soundZones[i].setSize( ofGetWidth()/2, ofGetHeight()/2 );
		stringstream ss;
		ss<<i;
		soundZones[i].name = ss.str();
	}


	//Shuffle zones (animals+songs+answers)
	shuffleZones();


	//
	ofSetVerticalSync(true);
	ofSetFrameRate(60);
	ofBackground(255,255,255);

	std::cout<<"end setup"<<endl;

}
void ScreenGame::load_language_data()
{
	loadCommonStrings(language);

	//now load that same file and get the values out
	ofxXmlSettings settings;
	settings.loadFile("strings.xml");
	explanationStr = settings.getValue(language + ":explanation", "Mou la mà per jugar!");

}

void ScreenGame::do_specific_things()
{
	for( int i=0; i<soundConf.size(); i++)
			{
				soundConf[i].sound.stop();
			}
}

//--------------------------------------------------------------
void ScreenGame::update(){

	next_screen = ScreenInfo();

	//Updates the question and explanation buttons
	updateExplanation();
		if( state == STATE_EXPLANATION ) return;

	menuButton.updateSelectProgress();
	if( menuButton.selected )
	{
		reset();
		menuButton.reset();
		next_screen = ScreenInfo(SCREEN_WELCOME, language);
	}



	if( !haveWinner() && state == STATE_PLAYING )
	{
		for( int i=0; i<soundZones.size(); i++ )
		{
			soundZones[i].updateStatus( dyngest->push && CurrTime() > 200 );
		}
		if( dyngest->push ) ZeroTime() ;

		/*
		 * Check if a wrong answer was selected
		 */
		for( int i=0; i<soundZones.size(); i++ )
		{
			if( soundZones[i].selected && i != currentAnswer && animX[0].isCompleted() )
			{
				wrongSound.play();
				soundZones[i].selected = false;
				life--;
				heart.setLife(life);
				if( life == 0 )
				{
//					next_screen = ScreenInfo(SCREEN_REPLAY, language);
					state = STATE_REPLAY;
					return;
				}
			}
		}

		/*
		 * Check if answer was correct. If so, animate!
		 */
		if( soundZones[currentAnswer].selected &&  animX[0].isCompleted() )
		{

			for( int i=0; i<soundZones.size(); i++ )
			{
				if( i==currentAnswer)
				{
					animX[i].setParameters(i*4,  easingquad,ofxTween::easeOut, soundZones[currentAnswer].pos.x, 450/*ofGetWidth()*0*/, animDuration, 0.0);
					animY[i].setParameters(i*4+1,easingquad,ofxTween::easeOut, soundZones[currentAnswer].pos.y, 170/*ofGetHeight()*0*/, animDuration, 0.0);
					animW[i].setParameters(i*4+2,easingquad,ofxTween::easeOut, soundZones[currentAnswer].siz.x, FRAME_W*1.5/*ofGetWidth()*/, animDuration, 0.0);
					animH[i].setParameters(i*4+3,easingquad,ofxTween::easeOut, soundZones[currentAnswer].siz.y, FRAME_H*1.5, animDuration, 0.0);
					animX[i].update();
					animY[i].update();
					animW[i].update();
					animH[i].update();
				}
				else
				{
					animX[i].setParameters(i*4,  easingquad,ofxTween::easeOut, soundZones[i].pos.x, animPosOut[i].x, animDuration, 0.0);
					animY[i].setParameters(i*4+1,easingquad,ofxTween::easeOut, soundZones[i].pos.y, animPosOut[i].y, animDuration, 0.0);
					animW[i].setParameters(i*4+2,easingquad,ofxTween::easeOut, soundZones[i].siz.x, soundZones[i].siz.x, animDuration, 0.0);
					animH[i].setParameters(i*4+3,easingquad,ofxTween::easeOut, soundZones[i].siz.y, soundZones[i].siz.y, animDuration, 0.0);
					animX[i].update();
					animY[i].update();
					animW[i].update();
					animH[i].update();
					animX[i].update();
					animY[i].update();
					animW[i].update();
					animH[i].update();
				}
			}

			correctSound.play();
			soundZones[currentAnswer].selected = false;
			can_shuffle = true;
			points++;
			bar.setPoints(points);
		}



	}

	//Update animation of zones
	for( int i=0; i<soundZones.size(); i++ )
	{
		soundZones[i].setPosSize( ofVec2f(animX[i].update(), animY[i].update()), ofVec2f( animW[i].update(), animH[i].update() ) );
	}


	//if animation finished, shuffle questions
	if( animX[0].isCompleted() && can_shuffle )
	{
		//        reset();
		shuffleZones();
		can_shuffle = false;
	}

	/*
	 * Play again after winning or losing, trigger using a swipe
	 */
	if(  (  state != STATE_PLAYING ) &&
				dyngest->push 			&&
				CurrTime() > 200 )
		{
			swipeSound.play();
			ZeroTime();

			/*if( state == STATE_EXPLANATION )
			{
				state = STATE_PLAYING;
				return;
			}*/
			next_screen = ScreenInfo(SCREEN_WELCOME, language);
			reset();
		}


}

//--------------------------------------------------------------
void ScreenGame::draw(){



	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glDisable(GL_DEPTH_TEST);

	ofEnableAlphaBlending();

	if( state == STATE_EXPLANATION )
	{
		explanationImg.draw(0,0);
		returnButton.display();
		return;
	}


	ofSetColor(255,255);
	bg.draw(0,0,ofGetWidth(), ofGetHeight());
	leaves_bottom.draw(0,0,ofGetWidth(), ofGetHeight());
	leaves_top.draw(0,0,ofGetWidth(), ofGetHeight());
	ofSetColor(255);


	/*
	 * Draw zones (animals)
	 */
	for( int i=0; i<soundZones.size(); i++ )
		if(i!=currentAnswer)
			soundZones[i].draw();

	//Draw correct on top
	soundZones[currentAnswer].draw();



	ofxTrueTypeFontUC * usefont = &font;
	if( language == LANG_RUS)
		usefont = &fontRUS;


	if( state == STATE_WINNER )
	{
		float imgSize = winnerSize.update();
		winner_bg.draw(ofGetWidth()/2, ofGetHeight()/2, ofGetWidth()*imgSize, ofGetHeight()*imgSize);
		ofSetColor(0);
//				usefont->drawString( winnerStr, ofGetWidth()*0.3,  ofGetHeight()*0.4  );

				drawMultilineString(winnerStr, ofVec2f(ofGetWidth()*0.5,  ofGetHeight()*0.5), 400, *usefont, true);

				ofSetColor(255);
				winner_icon.draw(ofGetWidth()/2, ofGetHeight()*0.65);


		if( playWinnerSound && !winnerSound.getIsPlaying() )
		{
			winnerSound.play();
			playWinnerSound = false;
		}
	}

	if( state == STATE_REPLAY )
	{
		float replaySize = winnerSize.update();
		replay_bg.draw(ofGetWidth()/2, ofGetHeight()/2, ofGetWidth()*replaySize, ofGetHeight()*replaySize);
		ofSetColor(0);
//		usefont->drawString( replayStr, ofGetWidth()*0.3,  ofGetHeight()*0.4  );
		drawMultilineString(replayStr, ofVec2f(ofGetWidth()*0.5,  ofGetHeight()*0.5), 650, *usefont, true);

		ofSetColor(255);
		replay_icon.draw(ofGetWidth()/2, ofGetHeight()*0.65);
	}

	bar.display( ofVec2f(1668,519) );
	heart.display( ofVec2f(1678,225) );
	menuButton.display();
	helpButton.display();
}


void ScreenGame::shuffleZones()
{
	std::cout<<"Shuffling!"<<std::endl;

	for( int i=0; i<soundConf.size(); i++)
			{
				soundConf[i].sound.stop();
			}


	do
	{
		std::random_shuffle(soundConf.begin(),soundConf.end());

		currentAnswer = std::rand()%4;
	}while( usedSounds.count(soundConf[currentAnswer].name )   );

	//Add to used sounds
	usedSounds[ soundConf[currentAnswer].name ] = 1;

	for(int i=0; i<soundZones.size(); i++ )
	{
		animX[i].setParameters(i*4,  easingquad,ofxTween::easeOut, soundZones[i].pos.x, animPosIn[i].x, animDuration, 0.0);
		animY[i].setParameters(i*4+1,easingquad,ofxTween::easeOut, soundZones[i].pos.y, animPosIn[i].y, animDuration, 0.0);
		animW[i].setParameters(i*4+2,easingquad,ofxTween::easeOut, soundZones[i].siz.x, FRAME_W/*ofGetWidth()*0.5*/,  animDuration, 0.0);
		animH[i].setParameters(i*4+3,easingquad,ofxTween::easeOut, soundZones[i].siz.y, FRAME_H/*ofGetHeight()*0.5*/, animDuration, 0.0);
		animX[i].update();
		animY[i].update();
		animW[i].update();
		animH[i].update();
		soundZones[i].selected = false;
		soundZones[i].setAnswer( i == currentAnswer ) ;
		soundZones[i].setFrame( &frames[i] );
		if( i<4 )
			soundZones[i].setImage( &(soundConf[i].images[i]) );
		else
			soundZones[i].setImage( &(soundConf[i].images[0]) );

	}

}

void ScreenGame::reset()
{
	life = INIT_LIFE;
	playWinnerSound = true;
	points = 0;
	usedSounds.clear();
	bar.setPoints(0);
	heart.setLife(INIT_LIFE);
	shuffleZones();
	state = STATE_PLAYING;
	winnerSize.setParameters(2,easingelastic,ofxTween::easeOut, 0.0, 1.0, 2000.0, 1500.0);
	ZeroTime();
}


bool ScreenGame::haveWinner()
{
	if( points >= WIN_POINTS ) state = STATE_WINNER;
	return points >= WIN_POINTS;
}

//--------------------------------------------------------------
void ScreenGame::keyPressed(int key){

	switch(key) {


	case 'f':
		ofToggleFullscreen();
		break;

	case 's':
		shuffleZones();
		break;
	}


}


//
////--------------------------------------------------------------
//void ScreenGame::keyReleased(int key){
//
//}
//
////--------------------------------------------------------------
//void ScreenGame::mouseMoved(int x, int y ){
//
//}
//
////--------------------------------------------------------------
//void ScreenGame::mouseDragged(int x, int y, int button){
//}
//
////--------------------------------------------------------------
//void ScreenGame::mousePressed(int x, int y, int button){
//}
//
////--------------------------------------------------------------
//void ScreenGame::mouseReleased(int x, int y, int button){
//}
//
////--------------------------------------------------------------
//void ScreenGame::windowResized(int w, int h){
//
//}
//
////--------------------------------------------------------------
//void ScreenGame::gotMessage(ofMessage msg){
//
//}
//
////--------------------------------------------------------------
//void ScreenGame::dragEvent(ofDragInfo dragInfo){
//
//}
//



